<?php namespace Controllers\Account;

use AuthorizedController;
use Redirect;
use View;
use Validator;
use Input;
use Sentry;

use Partner;

class DashboardController extends AuthorizedController {

	/**
	 * Redirect to the profile page.
	 *
	 * @return Redirect
	 */
	public function getIndex()
	{
		// Redirect to the profile page
		return Redirect::route('profile');
	}
	
	public function getBecomeDev()
	{
		if(Sentry::getUser()->isAppliedFor())
			return Redirect::route('profile')->with('info', 'You already applied for partnership.');
		
		if(Sentry::getUser()->isDev())
			return Redirect::to('/')->with('info', 'You are already a developer!');
		else
			return View::make('frontend/account/becomedev');
	}
	
	public function postBecomeDev()
	{
		// Declare the rules for the form validation
		$rules = array(
			'idea' 		 => 'required|max:255'
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Grab the user
		$user = Sentry::getUser();
		
		$partnership = Partner::where('user_id', $user->id)->first();
		
		// If user already applied for partnership
		if(!is_null($partnership))
		{
			return Redirect::route('profile')->with('info', 'You already applied for partnership.');
		}
		
		$partnership = new Partner;
		
		$partnership->user_id = $user->id;
		$partnership->idea = e(Input::get('idea'));
		$partnership->approved = false;
		
		$partnership->save();

		// Redirect to the settings page
		return Redirect::route('profile')->with('success', 'You have successfully applied for partnership.');
	}

}
