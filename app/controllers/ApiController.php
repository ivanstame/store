<?php

class ApiController extends BaseController {

	public function getSlugged($string = null)
	{
		return $string == null ? "" : Str::slug($string);
	}
}
