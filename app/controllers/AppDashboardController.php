<?php

class AppDashboardController extends AuthController {

    public function getApps()
    {
        $apps = Sentry::getUser()->apps;

        return View::make('frontend.account.devapps', compact('apps'));
    }
}
