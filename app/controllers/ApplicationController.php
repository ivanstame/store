<?php

class ApplicationController extends BaseController {

	public function getApp($slug = null, $versionId = null)
	{
		$app = Application::where('slug', $slug)->where('approved', true)->first();

		if (is_null($app))
		{
			return App::abort(404);
		}

		$version = Version::find($versionId == null ? $app->version_id : $versionId);

		if (is_null($version))
		{
			return App::abort(404);
		}

		$bugs = Bug::where('app_id', $app->id)->get();

		return View::make('frontend/app/app', compact('app', 'version', 'bugs'));
	}

	public function postComment($slug)
	{
		// The user needs to be logged in, make that check please
		if ( ! Sentry::check())
		{
			return Redirect::to("app/$slug#!comments")->with('error', 'You need to be logged in to post comments!');
		}

		$app = Application::where('slug', $slug)->first();

		$rules = array(
			'comment' => 'required|min:3',
		);

		// Create a new validator instance from our dynamic rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now
		if ($validator->fails())
		{
			return Redirect::to("app/$slug#!comments")->withInput()->withErrors($validator);
		}

		// Save the comment
		$comment = new AppComment;
		$comment->user_id = Sentry::getUser()->id;
		$comment->content = e(Input::get('comment'));
		$comment->app_id = $app->id;
		$comment->parent_id = 0;
		$comment->ip = Request::getClientIp();

		// Was the comment saved with success?
		if($comment->save())
		{
			return Redirect::to("app/$slug#!comments")->with('success', 'Your comment was successfully added.');
		}

		return Redirect::to("app/$slug#!comments")->with('error', 'There was a problem adding your comment, please try again.');
	}

	public function getDownloadApp($slug = null, $versionId = null)
	{
		if(Config::get('store.modules.downloads') == false)
			return App::abort(403);

		if (is_null($app = Application::where('slug', $slug)->first()))
		{
			return App::abort(404);
		}

		// If version is not specified download the latest one if the is latest one
		$version = Version::where('id', $versionId)->first();

		if(is_null($version))
		{
			return App::abort(404);
		}

		// Check if we can download it if it's not free
		if(!$app->isFree())
		{
			if(Sentry::check())
			{
				if(!$app->isPurchased())
				{
					return Redirect::to('app/' . $app->slug . '/buy');
				}
			}
			else
			{
				return Redirect::to('auth/singin');
			}
		}

		$downloadString = route('download', array(Helpers::safe_encrypt($app->slug . ':' . $version->id)));

		return View::make('frontend/app/download', compact('app', 'version', 'downloadString'));
	}

	public function getDownload($downloadString = null, $from = null)
	{
		if(Config::get('store.modules.downloads') == false)
			return App::abort(403);

		if(is_null($downloadString))
		{
			return App::abort(404);
		}

		// When deecrypted explode it, make it array
        // $param[0] = app slug
        // $param[1] = version id
        $params = explode(':', Helpers::safe_decrypt($downloadString));

        if (is_null($app = Application::where('slug', $params[0])->first()))
		{
			return App::abort(404);
		}

		if (is_null($version = Version::where('id', $params[1])->first()))
		{
			return App::abort(404);
		}

		// If app is not fre, we must be sure user have it purchased
		// We have that code already, but better be safe then sorry
		if(!$app->isFree())
		{
			if(Sentry::check())
			{
				if(!$app->isPurchased())
				{
					return Redirect::to('app/' . $app->slug . '/buy');
				}
			}
			else
			{
				return Redirect::to('auth/singin');
			}
		}

		if (is_null($download = Download::where('download_string', $downloadString)->first()))
		{
			$download = new Download;

			$download->download_string = $downloadString;
			$download->version_id = $version->id;
			$download->user_id = Sentry::check() ? Sentry::getId() : 0;
			$download->downloads = $from == null ? 1 : 0;
			$download->downloads_client = $from != null ? 1 : 0;
			$download->ip = Request::getClientIp();

			$download->save();
		}
		else // We have that download recorded
		{
			// We are checking if 2h are passed, if so, redirect to download page again...
			$time = strtotime($download->created_at) - strtotime(date('Y-m-d H:i:s'));
			if((abs($time) / 3600) >= 2)
				return Redirect::to('app/' . $app->slug . '/' . $version->id . '/download')->with('info', 'Link has expired, but we will generate one for you.');

			// If link is still valid

			if(is_null($from))
				$download->downloads = $download->downloads + 1;
			else
				$download->downloads_client = $download->downloads_client + 1;

			$download->ip = Request::getClientIp();

			$download->save();
		}

		return Response::download(base_path() . '\uploads\files\apps\\' . $app->id . '\\' . $version->filename);
	}

	public function getApplyApp()
	{
		if(Config::get('store.modules.app_submission') == false)
			return App::abort(403);

		$question = Question::where('active', '=', true)->orderBy(DB::raw('RAND()'))->first();
		$categories = Category::where('top_level', true)->get();

		return View::make('frontend.app.add', compact('question', 'categories'));
	}

	public function postApplyApp()
	{
		if(Config::get('store.modules.app_submission') == false)
			return App::abort(403);

		$user = Sentry::getUser();

		if($user->canSubmitApp())
		{
			$input = Input::get();

	        $rules = array(
	        	'name'              => 'required|max:50|unique:store_apps|min:2|regex:/^[A-Za-z][A-Za-z0-9-_\s.:]*$/',
                'short_info'        => 'required|min:20|max:255',
                'slug'				=> 'required|regex:/^([a-z0-9]+-)*[a-z0-9]+$/i|unique:store_apps',
                'long_info'         => 'required|min:100',
                'developer'         => 'required|min:2|max:25',
                'developer_email'   => 'required|email|max:255',
                'icon'              => 'mimes:jpg,jpeg,png',
                'price'             => 'required|numeric',
                'homepage'          => 'required|url',
                'version'			=> 'required',
                'changelog'			=> 'required',
                'licence'			=> 'required',
                'installer'			=> 'mimes:exe,msi,zip',
                'answer'           	=> 'required|question:' . Input::get('qid')
	        );

	        $validator = Validator::make($input, $rules);

            if ($validator->fails())
                return Redirect::back()->withInput()->withErrors($validator);

            # This is bad code, make sure you revist this section in future
            if(is_null(Input::file('icon')))
            {
            	$this->messageBag->add('icon', 'Icon file is required!');
            	return Redirect::back()->withInput()->withErrors($this->messageBag);
            }

            if(is_null(Input::file('installer')))
            {
            	$this->messageBag->add('installer', 'Executable file is required!');
            	return Redirect::back()->withInput()->withErrors($this->messageBag);
            }

            $need_key = false;
            if(Input::get('need_key') == 'need')
                $need_key = true;

            Eloquent::unguard();
            $app = Application::create(array(
            	'category_id' 	=> Input::get('category'),
	            'user_id' 		=> Sentry::getId(),
	            'topic_id'		=> 0,
	            'need_key' 		=> $need_key,
	            'version_id' 	=> 0,
	            'name' 			=> e(Input::get('name')),
	            'short_info' 	=> e(Input::get('short_info')),
	            'info' 			=> '<div id="info">' . e(Input::get('long_info')) . '</div>',
	            'featured' 		=> false,
	            'developer' 	=> e(Input::get('developer')),
	            'icon' 			=> '',
	            'price' 		=> Input::get('price'),
	            'website' 		=> e(Input::get('homepage')),
	            'dev_email' 	=> e(Input::get('developer_email')),
	            'approved' 		=> false,
	            'downloads' 	=> 0,
	            'views' 		=> 0,
				'updated' 		=> date('Y-m-d H:i:s'),
	            'slug' 			=> e(Input::get('slug')),
	            'is_in_client'	=> false,
	            'tags' 			=> Input::get('tags')
	        ));

			$user->postAddApp();

			# Create a dir for the app

			if(!file_exists('uploads/files/apps/' . $app->slug))
				mkdir('uploads/files/apps/' . $app->id, 0777, true);

			$icoExtension = Input::file('icon')->getClientOriginalExtension();
			$icoFilename = $app->slug . '.' . $icoExtension;
			Input::file('icon')->move(public_path() . '/assets/app_icons', $icoFilename);

			$version = Version::create(array(
				'app_id'			=> $app->id,
				'version'	 		=> e(Input::get('version')),
				'architecture'		=> Input::get('architecture'),
				'changelog'			=> e(Input::get('changelog')),
				'licence'			=> e(Input::get('licence')),
				'silent_arg'		=> e(Input::get('silence')),
				'original_filename'	=> Input::file('installer')->getClientOriginalName(),
				'filename'			=> '',
				'size'				=> Input::file('installer')->getSize(),
				'md5'				=> '',
				'order'				=> 1
			));

			$exeExtension = Input::file('installer')->getClientOriginalExtension();
			$exeFilename = $app->slug . '_' . Str::slug($version->version) . '.' . $exeExtension;
			Input::file('installer')->move('uploads/files/apps/' . $app->slug, $exeFilename);

			$app->version_id = $version->id;
			$app->updated = date('Y-m-d H:i:s');
			$app->icon = 'assets/app_icons/' . $icoFilename;
			$app->save();

			$version->md5 = md5_file('uploads/files/apps/' . $app->slug . '/' . $exeFilename);
			$version->filename =  $exeFilename;
			$version->save();

			return Redirect::to('/')->with('success', 'Your application is submitted for approval. We will be contacting you very soon.');
		}
	}

	public function getBuyApp($appSlug = null)
	{
		return "BUY";
	}

	public function getAddVerison($appSlug = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		return View::make('frontend.app.addversion', compact('app'));
	}

	public function postAddVerison($appSlug = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		$input = Input::get();

        $rules = array(
            'version'			=> 'required',
            'changelog'			=> 'required',
            'licence'			=> 'required',
            'installer'			=> 'mimes:exe,msi,zip'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails())
            return Redirect::back()->withInput()->withErrors($validator);

        # This is bad code, make sure you revist this section in future
        if(is_null(Input::file('installer')))
        {
        	$this->messageBag->add('installer', 'Executable file is required!');
        	return Redirect::back()->withInput()->withErrors($this->messageBag);
        }

		Eloquent::unguard();
        $version = Version::create(array(
			'app_id'			=> $app->id,
			'version'	 		=> e(Input::get('version')),
			'architecture'		=> Input::get('architecture'),
			'changelog'			=> e(Input::get('changelog')),
			'licence'			=> e(Input::get('licence')),
			'silent_arg'		=> e(Input::get('silence')),
			'original_filename'	=> Input::file('installer')->getClientOriginalName(),
			'filename'			=> '',
			'size'				=> Input::file('installer')->getSize(),
			'md5'				=> '',
			'order'				=> 1
		));

		$exeExtension = Input::file('installer')->getClientOriginalExtension();
		$exeFilename = $app->slug . '_' . Str::slug($version->version) . '.' . $exeExtension;
		Input::file('installer')->move('uploads/files/apps/' . $app->slug, $exeFilename);

		$app->version_id = Input::get('main') == 1 ? $version->id : $app->version_id;
		$app->updated = date('Y-m-d H:i:s');
		$app->save();

		$version->md5 = md5_file('uploads/files/apps/' . $app->slug . '/' . $exeFilename);
		$version->filename =  $exeFilename;
		$version->save();

		return Redirect::to('app/' . $app->slug)->with('success', 'New version has been added to app.');
	}

	public function getEditApp($appSlug = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		return View::make('frontend.app.editapp', compact('app'));
	}

	public function postEditApp($appSlug = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		$input = Input::get();

		$rules = array(
            'short_info'        => 'required|min:20|max:255',
            'slug'				=> 'required|regex:/^([a-z0-9]+-)*[a-z0-9]+$/i|unique:store_apps,slug,' . $app->slug . ',slug',
        	'name'              => 'required|max:50|unique:store_apps,name,' . $app->name . ',name|min:2|regex:/^[A-Za-z][A-Za-z0-9-_\s]*$/',
            'long_info'         => 'required|min:100',
            'developer'         => 'required|min:2|max:25',
            'developer_email'   => 'required|email|max:255',
            'price'             => 'required|numeric',
            'homepage'          => 'required|url'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails())
            return Redirect::back()->withInput()->withErrors($validator);

        $need_key = false;
            if(Input::get('need_key') == 'need')
                $need_key = true;

		Eloquent::unguard();

		$app->name = e(Input::get('name'));
		$app->slug = e(Input::get('slug'));
		$app->short_info = e(Input::get('short_info'));
		$app->info = '<div id="info">' . e(Input::get('long_info')) . '</div>';
		$app->developer = e(Input::get('developer'));
		$app->dev_email = e(Input::get('developer_email'));
		$app->price = e(Input::get('price'));
		$app->website = e(Input::get('homepage'));
		$app->save();

		return Redirect::route('view-app', array(e(Input::get('slug'))))->with('success', 'You successfully edited your application!');
	}

	public function getEditVersion($appSlug = null, $versionId = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		$version = Version::where('id', $versionId)->first();
		if(is_null($version))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		return View::make('frontend.app.editversion', compact('app', 'version'));
	}

	public function postEditVersion($appSlug = null, $versionId = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		$version = Version::where('id', $versionId)->first();
		if(is_null($version))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		Eloquent::unguard();

		$version->version = e(Input::get('version'));
		$version->architecture = Input::get('architecture') == 64 ? 64 : 32;
		$app->version_id = Input::get('main') == "1" ? $version->id : $app->version_id;
		$version->silent_arg = e(Input::get('silence'));
		$version->changelog = e(Input::get('changelog'));
		$version->licence = e(Input::get('licence'));
		$version->save();
		$app->save();

		return Redirect::route('view-app', array($appSlug))->with('success', 'You successfully edited your version!');
	}

	public function getDeleteVersion($appSlug = null, $versionId = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		$version = Version::where('id', $versionId)->first();
		if(is_null($version))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		return Redirect::route('view-app', array($appSlug))->with('success', 'Version is successfully deleted!');
	}

	public function getAddScreenshot($appSlug = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		return View::make('frontend.app.addscreenshot', compact('app'));
	}

	public function postAddScreenshot($appSlug = null)
	{
		$app = Application::where('slug', $appSlug)->where('approved', true)->first();
		if(is_null($app))
			return App::abort(404);

		if($app->user_id != Sentry::getId() || !Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
			return Redirect::to('/')->with('info', 'You really tought that i wouldn\'t check if that is your app?');

		$input = Input::get();

        $rules = array(
            'caption'			=> 'max:32',
            'image'				=> 'mimes:png,jpg,jpeg'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails())
            return Redirect::back()->withInput()->withErrors($validator);

        # This is bad code, make sure you revist this section in future
        if(is_null(Input::file('image')))
        {
        	$this->messageBag->add('image', 'Image file is required!');
        	return Redirect::back()->withInput()->withErrors($this->messageBag);
        }

		Eloquent::unguard();
        $screenshot = Screenshot::create(array(
			'caption'			=> e(Input::get('caption')),
			'app_id'			=> $app->id,
			'filename'	 		=> "",
			'order'				=> 0
		));

		$screenshotExtension = Input::file('image')->getClientOriginalExtension();
		$screenshotFilename = $app->slug . '_' . $screenshot->id . '.' . $screenshotExtension;
		Input::file('image')->move(public_path() . '/assets/app_screenshots', $screenshotFilename);

		$screenshot->filename = $screenshotFilename;
		$screenshot->save();

		return Redirect::route('view-app', array($appSlug))->with('success', 'You successfully added a screenshot!');
	}
}
