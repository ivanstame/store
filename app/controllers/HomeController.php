<?php

class HomeController extends BaseController {

	public function getIndex()
	{
		$downloaded =   Application::where('approved', '=', true)->where('version_id', '!=', 0)->orderBy('downloads', 'desc')->take(5)->get();
    	$popular    =   Application::where('approved', '=', true)->where('version_id', '!=', 0)->orderBy('views', 'desc')->take(5)->get();

		return View::make('frontend/home', compact('downloaded', 'popular'));
	}

	public function getAbouUs()
	{
		return View::make('frontend/about-us');
	}

	public function getSearch($query = null)
    {
        // Remove blank spaces on the edge
        $query = ltrim($query);
        $query = rtrim($query);

        // Get back from HTML to normal
        $query = urldecode($query);

        // Split into words(keywords)
        //$keywords = explode(' ', $query);
        $keywords = $this->multiExplode(array(' ', '+'), $query);

        // Query variable
        $q = "";

        foreach ($keywords as $key => $value)
        {
            if(strlen($value) > 0)
                $q .= "slug LIKE '%$value%' OR tags LIKE '%$value%' OR short_info LIKE '%$value%' OR ";

        }

        // Remove extra OR
        $q = substr($q, 0, (strlen($q) - 3));

        $whole = "SELECT * FROM store_apps WHERE approved=true AND version_id !=0 AND ($q)";

        $apps = DB::select($whole);

        return View::make('frontend/search')->with('apps', $apps)->with('query', $query);
    }

    public function postSearch()
    {
        return Redirect::to('search/' . urlencode(Input::get('query')));
    }

	public function multiExplode($delimiters, $string)
	{
	    return explode($delimiters[0],strtr($string,array_combine(array_slice($delimiters,1),array_fill(0,count($delimiters)-1,array_shift($delimiters)))));
	}

    public function getWindows()
    {
        $winCategory    = Category::where('slug', 'windows')->first();
        $apps           = Application::with('category')->paginate(10);

        return View::make('frontend.os')->with('category', $winCategory)->with('apps', $apps)->with('hard', true);
    }

    public function getLinux()
    {
        $winCategory    = Category::where('slug', 'linux')->first();
        $apps           = Application::with('category')->paginate(10);

        return View::make('frontend.os')->with('category', $winCategory)->with('apps', $apps)->with('hard', true);
    }

    public function getMac()
    {
        $winCategory    = Category::where('slug', 'mac')->first();
        $apps           = Application::with('category')->paginate(10);

        return View::make('frontend.os')->with('category', $winCategory)->with('apps', $apps)->with('hard', true);
    }

    public function getCategory($parent = null, $child = null)
    {
        $parentCategory = Category::where('slug', $parent)->where('top_level', true)->first();
        if($parentCategory === null)
            return App::abort(404);

        $childCategory = Category::where('parent_id', $parentCategory->id)->where('slug', $child)->first();
        if($childCategory === null)
            return App::abort(404);

        $apps = DB::table('store_apps')->where('category_id', $childCategory->id)->paginate(10);

        return View::make('frontend.os')->with('category', $childCategory)->with('apps', $apps)->with('hard', false);
    }

}
