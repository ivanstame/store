<?php namespace Controllers\Admin;

use AdminController;
use View;
use Authentication;

class DashboardController extends AdminController {

	/**
	 * Show the administration dashboard page.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Show the page
		return View::make('backend/dashboard');
	}

	public function getLogins()
	{
		$records = Authentication::orderBy('updated_at', 'DESC')->paginate(5);

		return View::make('backend.logins', compact('records'));
	}

}
