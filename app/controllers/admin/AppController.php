<?php namespace Controllers\Admin;

use AdminController;
use Sentry;
use Application;
use Input;
use User;
use Version;
use View;
use Helpers;
use Redirect;
use URL;

class AppController extends AdminController
{

    public function getRequests()
    {
        $apps = Application::where('approved', false)->orderBy('created_at', 'ASC')->get();
        return View::make('backend.apps.requests', compact('apps'));
    }


    public function postRequest()
    {
        if(Input::get('action') == 'delete')
        {
            $app = Application::where('id', '=', Input::get('app_id'))->first();
            $version = Version::where('app_id', $app->id)->first();
            $user = User::where('id', $app->user_id)->first();

            #Send email here

            // Because it's rejected delete it.

            if($user->project_limits != -1)
            {
                $user->project_limits = $user->project_limits + 1;
                $user->save();
            }

            Helpers::deleteDir('uploads/files/apps/' . $app->id);
            unlink($app->icon);
            $app->forceDelete();
            $version->forceDelete();


            return Redirect::to('/admin/app/requests');
        }
        else
        {
            $app = Application::where('id', '=', Input::get('app_id'))->first();

            #Send email here

            $app->approved = true;
            $app->save();

            //$this->addforum($app->id);
            return Redirect::to('/admin/app/requests');
        }
    }
}
