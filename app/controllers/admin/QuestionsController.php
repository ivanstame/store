<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Question;
use Redirect;
use Sentry;
use Str;
use Validator;
use View;

class QuestionsController extends AdminController
{
	public function getIndex()
	{
		$questions = Question::orderBy('created_at', 'DESC');

		// Do we want to include the deleted users?
		if (Input::get('withTrashed'))
		{
			$questions = $questions->withTrashed();
		}
		else if (Input::get('onlyTrashed'))
		{
			$questions = $questions->onlyTrashed();
		}

		// Paginate the users
		$questions = $questions->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
			));


		return View::make('backend/questions/index', compact('questions'));
	}

	public function getCreate()
	{
		return View::make('backend/questions/create');
	}

	public function postCreate()
	{
		$rules = array(
			'question' 	=> 'required|max:128|unique:store_questions',
			'answer' 	=> 'required|min:1'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$question = new Question;

			$question->question = e(Input::get('question'));
			$question->answer = e(Str::lower(Input::get('answer')));
			$question->answered = 0;
			$question->failed = 0;
			$question->active = Input::get('active') == "1" ? true : false;

			$question->save();

			return Redirect::to('admin/questions')->with('success', 'You have successfully created a security question.');
		}

		return Redirect::to('admin/questions/create')->withInput()->withErrors($validator);
	}

	public function getEdit($id = null)
	{
		if (is_null($question = Question::find($id)))
		{
			return Redirect::to('admin/questions')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		return View::make('backend/questions/edit', compact('question'));
	}

	public function postEdit($id = null)
	{
		if (is_null($question = Question::find($id)))
		{
			return Redirect::to('admin/questions')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		$rules = array(
			'question' 	=> 'required|max:128|unique:questions,question,' . $id,
			'answer' 	=> 'required|min:1'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{

			$question->question = e(Input::get('question'));
			$question->answer = e(Str::lower(Input::get('answer')));
			$question->active = Input::get('active') == "1" ? true : false;

			if($question->save())
			{
				return Redirect::to("admin/questions/$id/edit")->with('success', Lang::get('admin/blogs/message.update.success'));
			}

			return Redirect::to('admin/questions')->with('success', 'You have successfully created a security question.');
		}

		return Redirect::to('admin/questions/create')->withInput()->withErrors($validator);
	}

	public function getDelete($id = null)
	{
		if (is_null($question = Question::find($id)))
		{
			return Redirect::to('admin/questions')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		$question->delete();

		return Redirect::to('admin/questions')->with('success', Lang::get('admin/blogs/message.delete.success'));
	}

	public function getRestore($id = null)
	{
		if (is_null($question = Question::withTrashed()->find($id)))
		{
			return Redirect::to('admin/questions')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		$question->restore();

		return Redirect::to('admin/questions')->with('success', Lang::get('admin/blogs/message.delete.success'));
	}

}
