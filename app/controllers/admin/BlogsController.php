<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Post;
use Redirect;
use Sentry;
use Str;
use Validator;
use View;
use BlogCategory;

class BlogsController extends AdminController {

	/**
	 * Show a list of all the blog posts.
	 *
	 * @return View
	 */
	public function getIndex()
	{

		$posts = Post::orderBy('created_at', 'DESC');

		// Do we want to include the deleted users?
		if (Input::get('withTrashed'))
		{
			$posts = $posts->withTrashed();
		}
		else if (Input::get('onlyTrashed'))
		{
			$posts = $posts->onlyTrashed();
		}

		// Paginate the users
		$posts = $posts->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
			));

		// Show the page
		return View::make('backend/blogs/index', compact('posts'));
	}

	/**
	 * Blog post create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		// Show the page
		return View::make('backend/blogs/create');
	}

	/**
	 * Blog post create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
		dd(Input::all());
		// Declare the rules for the form validation
		$rules = array(
			'title'   => 'required|min:3',
			'content' => 'required|min:3',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Create a new blog post
		$post = new Post;

		// Update the blog post data
		$post->title            = e(Input::get('title'));
		$post->slug             = e(Str::slug(Input::get('title')));
		$post->content          = e(Input::get('content'));
		$post->meta_title       = e(Input::get('meta-title'));
		$post->meta_description = e(Input::get('meta-description'));
		$post->meta_keywords    = e(Input::get('meta-keywords'));
		$post->user_id          = Sentry::getId();

		// Was the blog post created?
		if($post->save())
		{
			// Redirect to the new blog post page
			return Redirect::to("admin/blogs/$post->id/edit")->with('success', Lang::get('admin/blogs/message.create.success'));
		}

		// Redirect to the blog post create page
		return Redirect::to('admin/blogs/create')->with('error', Lang::get('admin/blogs/message.create.error'));
	}

	/**
	 * Blog post update.
	 *
	 * @param  int  $postId
	 * @return View
	 */
	public function getEdit($postId = null)
	{
		// Check if the blog post exists
		if (is_null($post = Post::find($postId)))
		{
			// Redirect to the blogs management page
			return Redirect::to('admin/blogs')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		// Show the page
		return View::make('backend/blogs/edit', compact('post'));
	}

	/**
	 * Blog Post update form processing page.
	 *
	 * @param  int  $postId
	 * @return Redirect
	 */
	public function postEdit($postId = null)
	{
		// Check if the blog post exists
		if (is_null($post = Post::find($postId)))
		{
			// Redirect to the blogs management page
			return Redirect::to('admin/blogs')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		// Declare the rules for the form validation
		$rules = array(
			'title'   => 'required|min:3',
			'content' => 'required|min:3',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Update the blog post data
		$post->title            = e(Input::get('title'));
		$post->slug             = e(Str::slug(Input::get('title')));
		$post->content          = e(Input::get('content'));
		$post->meta_title       = e(Input::get('meta-title'));
		$post->meta_description = e(Input::get('meta-description'));
		$post->meta_keywords    = e(Input::get('meta-keywords'));

		// Was the blog post updated?
		if($post->save())
		{
			// Redirect to the new blog post page
			return Redirect::to("admin/blogs/$postId/edit")->with('success', Lang::get('admin/blogs/message.update.success'));
		}

		// Redirect to the blogs post management page
		return Redirect::to("admin/blogs/$postId/edit")->with('error', Lang::get('admin/blogs/message.update.error'));
	}

	/**
	 * Delete the given blog post.
	 *
	 * @param  int  $postId
	 * @return Redirect
	 */
	public function getDelete($postId)
	{
		// Check if the blog post exists
		if (is_null($post = Post::find($postId)))
		{
			// Redirect to the blogs management page
			return Redirect::to('admin/blogs')->with('error', Lang::get('admin/blogs/message.not_found'));
		}

		// Delete the blog post
		$post->delete();

		// Redirect to the blog posts management page
		return Redirect::to('admin/blogs')->with('success', Lang::get('admin/blogs/message.delete.success'));
	}

	public function getCategories()
	{
		$categories = BlogCategory::orderBy('created_at');

		// Do we want to include the deleted users?
		if (Input::get('withTrashed'))
		{
			$categories = $categories->withTrashed();
		}
		else if (Input::get('onlyTrashed'))
		{
			$categories = $categories->onlyTrashed();
		}

		$categories = $categories->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
			));

		return View::make('backend/blogs/categories', compact('categories'));
	}

	public function getCategoryCreate()
	{
		return View::make('backend/blogs/createcategory');
	}

	public function postCategoryCreate()
	{
		// Declare the rules for the form validation
		$rules = array(
			'name'   => 'required|unique:blog_categories'
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		$category = new BlogCategory;
		$category->name = e(Input::get('name'));
		$category->slug = Str::slug(e(Input::get('name')));

		// Was the blog post created?
		if($category->save())
		{
			// Redirect to the new blog post page
			return Redirect::to("admin/blogs/categories")->with('success', Lang::get('admin/blogs/message.create.success'));
		}

		// Redirect to the blog post create page
		return Redirect::to('admin/blogs/categories/create')->with('error', Lang::get('admin/blogs/message.create.error'));
	}

	public function getCategoryEdit($id = null)
	{
		// Check if the blog post exists
		if (is_null($category = BlogCategory::find($id)))
		{
			// Redirect to the blogs management page
			return Redirect::to('admin/blogs/categories')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		// Show the page
		return View::make('backend/blogs/editcategory', compact('category'));
	}

	public function postCategoryEdit($id = null)
	{
		if (is_null($category = BlogCategory::find($id)))
		{
			// Redirect to the blogs management page
			return Redirect::to('admin/blogs/categories')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		$rules = array(
			'name'   => 'required' . (e(Input::get('name')) != $category->name ? '|unique:blog_categories' : ''),
			'slug'   => 'required' . (e(Input::get('slug')) != $category->slug ? '|unique:blog_categories' : '')
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		$category->name = e(Input::get('name'));
		$category->slug = e(Input::get('slug'));

		if($category->save())
		{
			return Redirect::to("admin/blogs/categories")->with('success', Lang::get('admin/blogs/message.create.success'));
		}

		return Redirect::to('admin/blogs/categories/create')->with('error', Lang::get('admin/blogs/message.create.error'));
	}

	public function getCategoryDelete($id = null)
	{
		if (is_null($category = BlogCategory::find($id)))
		{
			return Redirect::to('admin/blogs/categories')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		$category->delete();

		return Redirect::to('admin/blogs/categories')->with('success', Lang::get('admin/blogs/message.delete.success'));
	}

	public function getCategoryRestore($id = null)
	{
		if (is_null($category = BlogCategory::withTrashed()->find($id)))
		{
			return Redirect::to('admin/blogs/categories')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		$category->restore();

		return Redirect::to('admin/blogs/categories')->with('success', Lang::get('admin/blogs/message.delete.success'));
	}

}
