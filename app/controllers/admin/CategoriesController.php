<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Category;
use Redirect;
use Sentry;
use Str;
use Validator;
use View;

class CategoriesController extends AdminController {

	public function getIndex()
	{
		return View::make('backend/categories/index');
	}

	public function getEdit($id = null)
	{
		// Check if the category exists
		if (is_null($cat = Category::find($id)))
		{
			// Redirect to the category management page
			return Redirect::to('admin/categories')->with('error', Lang::get('admin/blogs/message.does_not_exist'));
		}

		// Show the page
		return View::make('backend/categories/edit', compact('cat'));
	}

	public function postEdit($id = null)
	{
		// Check if the blog post exists
		if (is_null($category = Category::find($id)))
		{
			// Redirect to the blogs management page
			return Redirect::to('admin/categories')->with('error', Lang::get('admin/categories/message.does_not_exist'));
		}

		$rules = array(
			'inputName' 		=> 'required|max:32',
			'inputParent' 		=> 'required',
			'inputDescrioption' => 'required',
			'inputKeywords' 	=> 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Update the category data
		$category->name = e(Input::get('inputName'));

		if(Input::get('inputParent') == 'none')
		{
			$category->parent_id = 0;
			$category->top_level = true;
		}
		else
		{
			$category->parent_id = e(Input::get('inputParent'));
			$category->top_level = false;
		}

		$category->description = e(Input::get('inputDescrioption'));
		$category->keywords = e(Input::get('inputKeywords'));
		$category->slug = Str::slug(e(Input::get('inputName')));
		$category->active = Input::get('active') == "1" ? true : false;

		// Was the category updated?
		if($category->save())
		{
			// Redirect to the new category page
			return Redirect::to("admin/categories/$id/edit")->with('success', Lang::get('admin/categories/message.update.success'));
		}

		// Redirect to the category management page
		return Redirect::to("admin/categories/$id/edit")->with('error', Lang::get('admin/categories/message.update.error'));
	}

	public function createCategory()
	{
		return View::make('backend/categories/create');
	}

	public function storeCategory()
	{
		$rules = array(
			'inputName' 		=> 'required|max:32',
			'inputParent' 		=> 'required',
			'inputDescrioption' => 'required',
			'inputKeywords' 	=> 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$category = new Category;
			$category->name = Input::get('inputName');

			if(Input::get('inputParent') == 'none')
			{
				$category->parent_id = 0;
				$category->top_level = true;
			}
			else
			{
				$category->parent_id = Input::get('inputParent');
				$category->top_level = false;
			}

			$category->description = Input::get('inputDescrioption');
			$category->keywords = Input::get('inputKeywords');
			$category->slug = Str::slug(Input::get('inputName'));
			$category->active = true;
			$category->save();

			return Redirect::to('admin/categories');

		}

		return Redirect::to('admin/categories/create')->withInput()->withErrors($validator);
	}

}
