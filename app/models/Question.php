<?php

class Question extends Eloquent
{
    protected $table = 'store_questions';


    public function created_at()
	{
		return ExpressiveDate::make($this->created_at)->getRelativeDate();
	}

	public function updated_at()
	{
		return ExpressiveDate::make($this->updated_at)->getRelativeDate();
	}

	public function deleted_at()
	{
		return ExpressiveDate::make($this->deleted_at)->getRelativeDate();
	}
}
