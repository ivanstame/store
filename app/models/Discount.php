<?php

class Discount extends Eloquent
{
    protected $table = 'discounts';

    public function app()
    {
        return $this->belongsTo('Application');
    }
    
    public function purchases()
    {
        return $this->hasMany('Purchase');
    }
}