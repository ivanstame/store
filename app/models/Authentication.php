<?php

class Authentication extends Eloquent {

    protected $table = 'store_authentications';

	public function user()
	{
		return $this->belongsTo('User');
	}

    public function created_at()
    {
        return ExpressiveDate::make($this->created_at)->getRelativeDate();
    }

    public function updated_at()
    {
        return ExpressiveDate::make($this->updated_at)->getRelativeDate();
    }
}
