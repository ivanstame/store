<?php

use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;

class User extends SentryUserModel {

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Returns the user full name, it simply concatenates
	 * the user first and last name.
	 *
	 * @return string
	 */
	public function fullName()
	{
		return "{$this->first_name} {$this->last_name}";
	}

	/**
	 * Returns the user Gravatar image url.
	 *
	 * @return string
	 */
	public function gravatar()
	{
		// Generate the Gravatar hash
		$gravatar = md5(strtolower(trim($this->gravatar)));

		// Return the Gravatar url
		return "//gravatar.org/avatar/{$gravatar}";
	}

	public function lastLogin()
    {
        return ExpressiveDate::make($this->last_login)->getRelativeDate();
    }

    public function created_at()
	{
		return ExpressiveDate::make($this->created_at)->getRelativeDate();
	}

	public function updated_at()
	{
		return ExpressiveDate::make($this->updated_at)->getRelativeDate();
	}

	public function apps()
    {
        return $this->hasMany('Application');
    }

	public function resets()
    {
        return $this->hasMany('Reset');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function searches()
    {
        return $this->hasMany('Search');
    }

    public function stats()
    {
        return $this->hasMany('Stat');
    }

    public function reports()
    {
        return $this->hasMany('Report');
    }

    public function bugs()
    {
        return $this->hasMany('Bug');
    }

    public function purchases()
    {
        return $this->hasMany('Purchase');
    }

    public function paypal_trans()
    {
        return $this->hasMany('PlayPal');
    }

    public function application_views()
    {
        return $this->hasMany('View');
    }

    public function downloads()
    {
        return $this->hasMany('Download');
    }

	public function partnership()
    {
        return $this->hasOne('Partner');
    }

	public function isAppliedFor()
	{
		$partnership = Partner::where('user_id', $this->id)->where('approved', false)->first();

        if(!is_null($partnership))
            return true;
        else
            return false;
	}

	public function isDev()
	{
		if($this->hasAnyAccess(array('admin', 'owner')))
            return true;

        $partnership = Partner::where('user_id', $this->id)->where('approved', true)->first();

        if($this->hasAnyAccess(array('admin', 'developer', 'owner')) && !is_null($partnership))
            return true;
        else
            return false;
	}

    // Add auth record, when user is logging in, with difference of 20 mins
    public function TraceMe()
    {
        $ip = Request::getClientIp();
        $auth = Authentication::where('user_id', $this->id)->orderBy('created_at', 'DESC')->first();

        if($auth > '0')
        {
            $then = strtotime($auth->updated_at);
            $now = strtotime(date("Y-m-d H:i:s"));

            $difference = ($now - $then);

            if($difference > '3600') // 1hour span time span
            {

                $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);
                $country = $xml->geoplugin_countryName;

                Eloquent::unguard();
                $auth = new Authentication;

                $auth->login_ip = $ip;
                $auth->login_country = $country;
                $auth->user_id = $this->id;

                $auth->save();
            }
            else
            {
                // Otherwise update the current visitor
                $auth->updated_at = date("Y-m-d H:i:s");
                $auth->save();
            }
        }
        else
        {
            $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);
            $country = $xml->geoplugin_countryName;

            Eloquent::unguard();
            $auth = new Authentication;

            $auth->login_ip = $ip;
            $auth->login_country = $country;
            $auth->user_id = $this->id;

            $auth->save();
        }
    }

    public function canSubmitApp()
    {
        return $this->project_limits > 0 || $this->project_limits == -1;
    }

    public function postAddApp()
    {
        if($this->project_limits != -1)
        {
            $this->project_limits = $this->project_limits - 1;
            $this->save();
        }
    }

}
