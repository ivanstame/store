<?php

class Search extends Eloquent
{
    protected $table = 'searches';

    public function user()
    {
        return $this->belongsTo('User');
    }
}