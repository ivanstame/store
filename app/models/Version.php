<?php

class Version extends Eloquent
{
    protected $table = 'store_versions';

    /**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

    public function app()
    {
        return $this->belongsTo('Application', 'app_id');
    }

    public function stats()
    {
        return $this->hasMany('Stat');
    }

    public function downloads()
    {
        return $this->hasMany('Download');
    }

    public function created_at()
    {
        return ExpressiveDate::make($this->created_at)->getRelativeDate();
    }
}
