<?php

class Serial extends Eloquent
{
    protected $table = 'store_serials';

    public function app()
    {
        return $this->belongsTo('Application');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function purchases()
    {
        return $this->hasMany('Purchase');
    }
}
