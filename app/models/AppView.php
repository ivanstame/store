<?php

class AppView extends Eloquent
{
    protected $table = 'store_views';

    /**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

    public function app()
    {
        return $this->belongsTo('Application');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }
}
