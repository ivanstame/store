<?php

class Bug extends Eloquent
{
    protected $table = 'store_bugs';

    public function app()
    {
        return $this->belongsTo('App');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function version()
    {
        return $this->belongsTo('Version');
    }

    public function created_at()
    {
        return ExpressiveDate::make($this->created_at)->getRelativeDate();
    }

    public function updated_at()
    {
        return ExpressiveDate::make($this->updated_at)->getRelativeDate();
    }
}
