<?php

class Reset extends Eloquent
{
    protected $table = 'resets';

    public function user()
    {
        return $this->belongsTo('User');
    }
}