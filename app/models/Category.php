<?php

use Illuminate\Routing;

class Category extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'store_categories';

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function childrens()
	{
		return Category::where('parent_id', '=', $this->id)->where('active', '=', true)->get();
	}

	public function apps()
    {
        return $this->hasMany('Application');
    }

    public function getParent()
    {
    	return Category::where('id', '=', $this->parent_id)->where('active', '=', true)->first();
    }

    public function isParent()
    {
        if($this->top_level)
            return true;
        else
            return false;
    }

    public function getLink()
    {
    	if($this->top_level)
    		return url($this->slug);
    	else
    		return url($this->getParent()->slug . '/' . $this->slug);
    }

}
