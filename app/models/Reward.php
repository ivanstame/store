<?php

class Reward extends Eloquent
{
    protected $table = 'rewards';

    public function app()
    {
        return $this->belongsTo('Application');
    }
}