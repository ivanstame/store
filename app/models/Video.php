<?php

class Video extends Eloquent
{
    protected $table = 'videos';
    
    public function app()
    {
        return $this->belongsTo('Application');
    }
}