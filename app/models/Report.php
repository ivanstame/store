<?php

class Report extends Eloquent
{
    protected $table = 'reports';

    public function app()
    {
        return $this->belongsTo('Application');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }
}