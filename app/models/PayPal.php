<?php

class PayPal extends Eloquent
{
    protected $table = 'paypal';
    
    public function user()
    {
        return $this->belongsTo('User');
    }

}