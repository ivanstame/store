<?php

class Stat extends Eloquent {
    
    protected $table = 'stats';
    
    public function app()
    {
        return $this->belongsTo('Application');
    }
    
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function version()
    {
        return $this->belongsTo('Version');
    }
}