<?php

class BlogView extends Eloquent
{
    protected $table = 'blog_views';

    public function post()
    {
        return $this->belongsTo('Post');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

}
