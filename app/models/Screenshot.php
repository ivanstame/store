<?php

class Screenshot extends Eloquent
{
    protected $table = 'store_screenshots';

    protected $softDelete = true;

    public function app()
    {
        return $this->belongsTo('Application');
    }
}
