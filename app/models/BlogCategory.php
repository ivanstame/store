<?php

use Illuminate\Routing;

class BlogCategory extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_categories';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	public function posts()
	{
		$ids = DB::table('blog_category_post')->lists('post_id');

		return DB::table('blog_posts')->whereIn('id', $ids)->get();
	}

    public function getLink()
    {
    	return url('blog/category/' . $this->slug);
    }

	public function created_at()
	{
		return ExpressiveDate::make($this->created_at)->getRelativeDate();
	}

	public function updated_at()
	{
		return ExpressiveDate::make($this->updated_at)->getRelativeDate();
	}

}
