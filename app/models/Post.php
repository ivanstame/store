<?php

class Post extends Eloquent {

	protected $table = 'blog_posts';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Deletes a blog post and all the associated comments.
	 *
	 * @return bool
	 */
	public function delete()
	{
		// Delete the comments
		$this->comments()->delete();

		// Delete the blog post
		return parent::delete();
	}

	/**
	 * Returns a formatted post content entry, this ensures that
	 * line breaks are returned.
	 *
	 * @return string
	 */
	public function content()
	{
		return nl2br($this->content);
	}

	/**
	 * Return the post's author.
	 *
	 * @return User
	 */
	public function author()
	{
		return $this->belongsTo('User', 'user_id');
	}

	/**
	 * Return how many comments this post has.
	 *
	 * @return array
	 */
	public function comments()
	{
		return $this->hasMany('Comment');
	}

	/**
	 * Return the URL to the post.
	 *
	 * @return string
	 */
	public function url()
	{
		return URL::route('view-post', $this->slug);
	}

	/**
	 * Return the post thumbnail image url.
	 *
	 * @return string
	 */
	public function thumbnail()
	{
		# you should save the image url on the database
		# and return that url here.
		return $this->thumbnail != "" ? $this->thumbnail : "http://www.placehold.it/170x96";
	}

	/**
	 * Get the date the post was created.
	 *
	 * @return string
	 */
	public function date()
	{
		return ExpressiveDate::make($this->created_at)->getRelativeDate();
	}

	/**
	 * Returns the date of the blog post creation,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function created_at()
	{
		return ExpressiveDate::make($this->created_at)->getRelativeDate();
	}

	public function getCategory()
	{
		$connection = DB::table('blog_category_post')->where('post_id', $this->id)->first();

		return BlogCategory::where('id', $connection->category_id)->first();
	}

	/**
	 * Returns the date of the blog post last update,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function updated_at()
	{
		return ExpressiveDate::make($this->updated_at)->getRelativeDate();
	}

	public function views()
    {
        return $this->hasMany('BlogView');
    }

    public function addView()
    {
    	$ip = Request::getClientIp();

    	$visitor = BlogView::where('user_ip', $ip)->orderBy('created_at', 'DESC')->first();

    	if($visitor > '0')
    	{
    		$then = strtotime($visitor->updated_at);
			$now = strtotime(date("Y-m-d H:i:s"));

			$difference = ($now - $then);

			if($difference > '1200') // 20 minutes time span
			{

				$xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);
				$country = $xml->geoplugin_countryName;

				$visitor = new BlogView;

				$visitor->user_ip = $ip;
				$visitor->country = $country;
				$visitor->user_id = Sentry::check() == true ? Sentry::getId() : 0;
				$visitor->post_id = $this->id;

				$visitor->save();
			}
			else
			{
				// Otherwise update the current visitor
				$visitor->updated_at = date("Y-m-d H:i:s");
				$visitor->save();
			}
    	}
	    else
	    {
	    	$xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);
			$country = $xml->geoplugin_countryName;

			$visitor = new BlogView;

			$visitor->user_ip = $ip;
			$visitor->country = $country;
			$visitor->user_id = Sentry::check() == true ? Sentry::getId() : 0;
			$visitor->post_id = $this->id;

			$visitor->save();
	    }
    }

}
