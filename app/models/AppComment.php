<?php

class AppComment extends Eloquent {

	protected $table = 'store_comments';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Get the comment's content.
	 *
	 * @return string
	 */
	public function content()
	{
		return nl2br($this->content);
	}

	public function childrens()
	{
		return AppComment::where('parent_id', '=', $this->id)->get();
	}

	/**
	 * Get the comment's author.
	 *
	 * @return User
	 */
	public function author()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function app()
	{
		return $this->belongsTo('Application');
	}

	public function created_at()
	{
		return ExpressiveDate::make($this->created_at)->getRelativeDate();
	}

}
