<?php

class Application extends Eloquent
{
    protected $table = 'store_apps';

    protected $softDelete = true;

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function screenshots()
    {
        return $this->hasMany('Screenshot', 'app_id');
    }

    public function stats()
    {
        return $this->hasMany('Stat');
    }

    public function rewards()
    {
        return $this->hasMany('Reward');
    }

    public function comments()
    {
        return $this->hasMany('AppComment', 'app_id');
    }

    public function commentsWhere()
    {
        return AppComment::where('app_id', $this->id)->where('parent_id', 0)->orderBy('created_at', 'DESC')->get();
    }

    public function videos()
    {
        return $this->hasMany('Video');
    }

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function system()
    {
        return $this->category()->getParent();
    }

    public function bugs()
    {
        return $this->hasMany('Bug');
    }

    public function coupons()
    {
        return $this->hasMany('Coupon');
    }

    public function discount()
    {
        return $this->hasMany('Discount');
    }

    public function purchases()
    {
        return $this->hasMany('Purchase', 'app_id');
    }

    public function versions()
    {
        return $this->hasMany('Version', 'app_id');
    }

    public function reports()
    {
        return $this->hasMany('Report');
    }

    public function views()
    {
        return $this->hasMany('AppView', 'app_id');
    }

    public function created_at()
    {
        return ExpressiveDate::make($this->created_at)->getRelativeDate();
    }

    public function updated_at()
    {
        return ExpressiveDate::make($this->updated_at)->getRelativeDate();
    }

    public function isFree()
    {
        if($this->price <= 0)
            return true;
        else
            return false;
    }

    public function needKeys()
    {
        return $this->need_key == true ? true : false;
    }

    public function haveKeysAvailable()
    {
        if($this->needKeys())
            return Serial::where('app_id', '=', $this->id)->where('used', '=', false)->count() > 0 ? true : false;
        else
            return true;
    }

    public function isPurchased($userId = null)
    {
        if( !is_null($userId) )
        {
            if(!$this->isFree())
            {
                $purchases = Purchase::where('user_id', $userId)->where('app_id', $this->id)->count();

                if($purchases != 0)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function addView()
    {
        $ip = Request::getClientIp();

        $visitor = AppView::where('user_ip', $ip)->orderBy('created_at')->first();

        if($visitor > '0')
        {
            $then = strtotime($visitor->updated_at);
            $now = strtotime(date("Y-m-d H:i:s"));

            $difference = ($now - $then);

            if($difference > '1200') // 20 minutes time span
            {

                $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);
                $country = $xml->geoplugin_countryName;

                $visitor = new AppView;

                $visitor->user_ip = $ip;
                $visitor->country = $country;
                $visitor->user_id = Sentry::check() == true ? Sentry::getId() : 0;
                $visitor->app_id = $this->id;

                $visitor->save();
            }
            else
            {
                // Otherwise update the current visitor
                $visitor->updated_at = date("Y-m-d H:i:s");
                $visitor->save();
            }
        }
        else
        {
            $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);
            $country = $xml->geoplugin_countryName;

            $visitor = new AppView;

            $visitor->user_ip = $ip;
            $visitor->country = $country;
            $visitor->user_id = Sentry::check() == true ? Sentry::getId() : 0;
            $visitor->app_id = $this->id;

            $visitor->save();
        }
    }

    public function getCrumbs()
    {
        echo "<li><a href=" . route('home') . ">Home</a> <span class=\"divider\"></span></li>";

        if($this->category->isParent())
        {
            echo "<li><a href=\"" . url($this->category->slug) . "\">" . $this->category->name . "</a> <span class=\"divider\"></span></li>";
            echo "<li class=\"active\">" . $this->name . "</li>";
        }
        else
        {
            echo "<li><a href=\"" . url($this->category->getParent()->slug) . "\">" . $this->category->getParent()->name . "</a> <span class=\"divider\"></span></li>";
            echo "<li><a href=\"" . url($this->category->getParent()->slug . '/' . $this->category->slug) . "\">" . $this->category->name . "</a> <span class=\"divider\"></span></li>";
            echo "<li class=\"active\">" . $this->name . "</li>";
        }
    }
}
