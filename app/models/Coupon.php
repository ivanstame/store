<?php

class Coupon extends Eloquent
{
    protected $table = 'coupons';

    public function app()
    {
        return $this->belongsTo('Application');
    }
    
    public function purchases()
    {
        return $this->hasMany('Purchase');
    }

}