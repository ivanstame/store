<?php

class Download extends Eloquent
{
    protected $table = 'store_downloads';

    protected $softDelete = true;

    public function version()
    {
        return $this->belongsTo('Version');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function total()
    {
        return $this->downloads + $this->downloads_client;
    }
}
