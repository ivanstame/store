<?php

class Purchase extends Eloquent
{
    protected $table = 'store_purchases';

    public function app()
    {
        return $this->belongsTo('Application');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function coupon()
    {
        return $this->belongsTo('Coupon');
    }

    public function discount()
    {
        return $this->belongsTo('Discount');
    }

    public function serial()
    {
        return $this->belongsTo('Serial');
    }

}
