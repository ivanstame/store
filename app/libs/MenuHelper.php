<?php

# I just like that i came up with this algo :D
class MenuHelper
{
	public static function makeMenu($node)
	{
		if($node->top_level)
		{
			echo "<li class=\"dropdown\">";
			echo "<a href=\"" . $node->getLink() . "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">$node->name<b class=\"caret\"></b></a>";
			echo "<ul class=\"dropdown-menu\">";
		}
		else
		{
			echo "<li>";
			echo "<a href=\"" . $node->getLink() ."\" ><span class=\"pull-right\">" . $node->apps->count() . "</span>$node->name</a>";
			echo "</li>";
		}

		foreach ($node->childrens() as $cat) {
			MenuHelper::makeMenu($cat);
		}

		if($node->top_level)
		{
			echo "</ul>";
			echo "</li>";
		}
	}

	public static function makeComments($node)
	{
		if($node->parent_id == 0)
		{
			echo "<div class=\"media\">";
			echo "<a class=\"pull-left\" href=\"#\">";
			echo "<img class=\"media-object\" alt=\"64x64\" style=\"width: 64px; height: 64px;\" src=\"" . $node->author->gravatar() . "\">";
			echo "</a>";
			echo "<div class=\"media-body\">";
			echo "<h4 class=\"media-heading\">" . $node->author->fullName() . " <small>" . $node->created_at() . "</small></h4>";
			echo $node->content();

		}
		else
		{
			echo "<div class=\"media\">";
			echo "<a class=\"pull-left\" href=\"#\">";
			echo "<img class=\"media-object\" alt=\"64x64\" style=\"width: 64px; height: 64px;\" src=\"" . $node->author->gravatar() . "\">";
			echo "</a>";
			echo "<div class=\"media-body\">";
			echo "<h4 class=\"media-heading\">" . $node->author->fullName() . " <small>" . $node->created_at() . "</small></h4>";
			echo $node->content();
			echo "</div>";
			echo "</div>";
			echo "<hr />";
		}

		foreach ($node->childrens() as $cat) {
			MenuHelper::makeComments($cat);
		}

		if($node->parent_id == 0)
		{
			echo "</div>";
			echo "</div>";
			echo "<hr />";
		}
	}

	public static function makeCategories($node)
	{
		if($node->top_level)
		{
			echo "<ul>";
			echo "<li><strong>$node->name</strong> (<a href=\"" . URL::to('admin/categories/' . $node->id . '/edit') . "\">Edit</a>) - (<a href=\"" . URL::to('admin/categories/' . $node->id . '/delete') . "\">Delete</a>)</li>";
			echo "<ul>";
		}
		else
		{
			echo "<li>$node->name  (<a href=\"" . URL::to('admin/categories/' . $node->id . '/edit') . "\">Edit</a>) - (<a href=\"" . URL::to('admin/categories/' . $node->id . '/delete') . "\">Delete</a>)</li>";
		}

		foreach ($node->childrens() as $cat) {
			MenuHelper::makeCategories($cat);
		}

		if($node->top_level)
		{
			echo "</ul>";
			echo "</ul>";
		}
	}

	public static function makeSelectCategories($node)
	{
		if($node->isParent())
		{
			echo "<option value=\"{$node->id}\">";
			echo "<b>" . $node->name . "</b>";
			echo "</option>";
		}
		else
		{
			echo "<option class=\"indented\" value=\"{$node->id}\">";
			echo "- " . $node->name . " (" . $node->getParent()->name . ")";
			echo "</option>";
		}

		foreach ($node->childrens() as $cat) {
			MenuHelper::makeSelectCategories($cat);
		}

	}
}

?>
