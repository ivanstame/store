@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Blog category managment ::
@parent
@stop

{{-- Page content --}}
@section('content')
	<div class="page-header">
		<h3>
			Blog category managment

			<div class="pull-right">
				<a href="{{ route('create/blogs-categories') }}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Create</a>
			</div>
		</h3>
	</div>

	<a class="btn btn-medium" href="{{ URL::to('admin/blogs/categories?withTrashed=true') }}">Include Deleted Categories</a>
	<a class="btn btn-medium" href="{{ URL::to('admin/blogs/categories?onlyTrashed=true') }}">Include Only Deleted Categories</a>

	{{ $categories->links() }}

	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th class="span1">Id</th>
				<th class="span2">Name</th>
				<th class="span2">Slug</th>
				<th class="span2">Created at</th>
				<th class="span2">Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($categories as $category)
				<tr>
					<td>{{ $category->id }}</td>
					<td>{{ $category->name }}</td>
					<td>{{ $category->slug }}</td>
					<td>{{ $category->created_at() }}</td>
					<td>
						<a href="{{ route('update/blog/category', $category->id) }}" class="btn btn-mini">@lang('button.edit')</a>
						@if ( ! is_null($category->deleted_at))
							<a href="{{ route('restore/blog/category', $category->id) }}" class="btn btn-mini btn-warning">@lang('button.restore')</a>
						@else
							<a href="{{ route('delete/blog/category', $category->id) }}" class="btn btn-mini btn-danger">@lang('button.delete')</a>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{{ $categories->links() }}
@stop
