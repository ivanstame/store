@extends('backend/layouts/default')

@section('title')
	Edit blog category ::
	@parent
@stop

@section('content')
<div class="page-header">
	<h3>
		Edit blog category

		<div class="pull-right">
			<a href="{{ route('blogs-categories') }}" class="btn btn-small btn-inverse"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<form class="form-horizontal" method="post" action="" autocomplete="off">
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
		<label class="control-label" for="name">Category name</label>
		<div class="controls">
			<input class="input-large" type="text" name="name" id="name" value="{{ Input::old('name', $category->name) }}" />
			{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
		</div>
	</div>

	<div class="control-group {{ $errors->has('slug') ? 'error' : '' }}">
		<label class="control-label" for="slug">Category slug</label>
		<div class="controls">
			<div class="input-prepend">
				<span class="add-on">
					{{ str_finish(URL::to('blog/category'), '/') }}
				</span>
				<input class="input-large" type="text" name="slug" id="slug" value="{{ Input::old('slug', $category->slug) }}">
			</div>
			{{ $errors->first('slug', '<span class="help-inline">:message</span>') }}
		</div>
	</div>

	<div class="control-group">
		<div class="controls">
			<a class="btn btn-link" href="{{ route('blogs-categories') }}">Cancel</a>

			<button type="reset" class="btn">Reset</button>

			<button type="submit" class="btn btn-success">Save</button>
		</div>
	</div>
</form>
@stop
