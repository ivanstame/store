@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
	@parent · Category management
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
	<h3>
		Category Management

		<div class="pull-right">
			<a href="{{ URL::to('admin/categories/create') }}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Create category</a>
		</div>
	</h3>
</div>
	@foreach (Category::where('top_level', true)->get() as $menu)
	    {{ MenuHelper::makeCategories($menu) }}
	@endforeach
@stop
