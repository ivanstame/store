@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
	@parent
	· Edit category settings
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Edit category

			<div class="pull-right">
				<a href="{{ route('categories') }}" class="btn btn-small btn-info"><i class="icon-circle-arrow-left icon-white"></i> Go back</a>
			</div>
		</h3>
	</div>
	<form class="form-horizontal" method="POST" action="" autocomplete="off">
		{{ Form::token() }}

		<div class="{{ $errors->has('inputName') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputName">Name</label>
			<div class="controls">
				<input type="text" id="inputName" placeholder="Category name" name="inputName" value="{{ Input::old('inputName', $cat->name) }}">
				<span class="help-inline">{{ $errors->first('inputName') }}</span>
			</div>
		</div>

		<div class="{{ $errors->has('inputParent') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputParent">Parent</label>
			<div class="controls">
				<select class="" id="inputParent" name="inputParent">
					@foreach(Category::all() as $cats)
						@if($cats->getParent())
							<option value="{{ $cats->id }}">{{ $cats->name . '(' . $cats->getParent()->name . ')' }}</option>
						@else
							<option value="{{ $cats->id }}">{{ $cats->name . '(' . 'top level' . ')' }}</option>
						@endif
					@endforeach
				</select>
				<small>
					@if($cat->getParent())
						Curent: {{ $cat->getParent()->name }}
					@endif
				</small>
				<span class="help-inline">{{ $errors->first('inputParent') }}</span>
			</div>
		</div>

		<div class="{{ $errors->has('inputDescrioption') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputDescrioption">Description</label>
			<div class="controls">
				<input type="text" id="inputDescrioption" placeholder="Meta description" name="inputDescrioption" value="{{ Input::old('inputDescrioption', $cat->description) }}">
				<span class="help-inline">{{ $errors->first('inputDescrioption') }}</span>
			</div>
		</div>

		<div class="{{ $errors->has('inputKeywords') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputKeywords">Keywords</label>
			<div class="controls">
				<input type="text" id="inputKeywords" placeholder="Meta keywords" name="inputKeywords" value="{{ Input::old('inputKeywords', $cat->keywords) }}">
				<span class="help-inline">{{ $errors->first('inputKeywords') }}</span>
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
			<label class="checkbox">
				<input type="checkbox" name="active" id="active" value="1" {{ $cat->active ? 'checked' : '' }}/> Active
			</label>
			</div>
		</div>

	  	<div class="control-group">
			<div class="controls">
		      	<button type="submit" class="btn btn-success">Create</button>
		      	<button type="reset" class="btn btn-inverse">Reset</button>
		    </div>
	  	</div>
	</form>
@stop

