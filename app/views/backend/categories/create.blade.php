@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
	@parent
	· Category creation
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Create a category

			<div class="pull-right">
				<a href="{{ URL::to('admin/categories') }}" class="btn btn-small btn-info"><i class="icon-circle-arrow-left icon-white"></i> Go back</a>
			</div>
		</h3>
	</div>
	<form class="form-horizontal" method="POST" action="{{ URL::to('admin/categories/create') }}">
		{{ Form::token() }}

		<div class="{{ $errors->has('inputName') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputName">Name</label>
			<div class="controls">
				<input type="text" id="inputName" placeholder="Category name" name="inputName">
				<span class="help-inline">{{ $errors->first('inputName') }}</span>
			</div>
		</div>

		<div class="{{ $errors->has('inputParent') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputParent">Parent</label>
			<div class="controls">
				<select class="" id="inputParent" name="inputParent">
					<option value="none">None (top level)</option>
					@foreach(Category::where('top_level', true)->get() as $cat)
						@if($cat->getParent())
							<option value="{{ $cat->id }}">{{ $cat->name . '(' . $cat->getParent()->name . ')' }}</option>
						@else
							<option value="{{ $cat->id }}">{{ $cat->name . '(' . 'top level' . ')' }}</option>
						@endif
					@endforeach
				</select>
				<span class="help-inline">{{ $errors->first('inputParent') }}</span>
			</div>
		</div>

		<div class="{{ $errors->has('inputDescrioption') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputDescrioption">Description</label>
			<div class="controls">
				<input type="text" id="inputDescrioption" placeholder="Meta description" name="inputDescrioption">
				<span class="help-inline">{{ $errors->first('inputDescrioption') }}</span>
			</div>
		</div>

		<div class="{{ $errors->has('inputKeywords') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="inputKeywords">Keywords</label>
			<div class="controls">
				<input type="text" id="inputKeywords" placeholder="Meta keywords" name="inputKeywords">
				<span class="help-inline">{{ $errors->first('inputKeywords') }}</span>
			</div>
		</div>

	  	<div class="control-group">
			<div class="controls">
		      	<button type="submit" class="btn btn-success">Create</button>
		      	<button type="reset" class="btn btn-inverse">Reset</button>
		    </div>
	  	</div>
	</form>
@stop

@section('scripts')
    @parent

    <script>
        $("#inputParent").select2({ minimumResultsForSearch: 20 });
    </script>
@stop
