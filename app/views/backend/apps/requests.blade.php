@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
    @parent · Application requests
@stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            Application waiting for approval
        </h3>
    </div>

    <div class="panel-group" id="accordion">
        @if(count($apps) == 0)
            <p>No apps waiting for approval!</p>
        @endif
        @foreach($apps as $app)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            {{ $app->name }}<small> submitted {{ $app->created_at() }}</small>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <b>Title</b>
                                            </td>
                                            <td>
                                                {{ $app->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Short info</b>
                                            </td>
                                            <td>
                                                {{ $app->short_info }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Developer</b>
                                            </td>
                                            <td>
                                                {{ $app->developer }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Email</b>
                                            </td>
                                            <td>
                                                {{ $app->developer_email }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Icon</b>
                                            </td>
                                            <td>
                                                <a href="{{ $app->icon }}">{{ $app->icon }}</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Website</b>
                                            </td>
                                            <td>
                                                <a href="{{ $app->website }}">{{ $app->website }}</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Slug</b>
                                            </td>
                                            <td>
                                                {{ $app->slug }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Tags</b>
                                            </td>
                                            <td>
                                                {{ $app->tags }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Created at</b>
                                            </td>
                                            <td>
                                                {{ $app->created_at }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 well">{{ $app->info }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3" style="margin-left: 20px;">
                                    <form action="" method="POST" role="form" class="form-inline">
                                        <div class="form-group">
                                            <input type="hidden" name="app_id" value="{{ $app->id }}" />
                                            <select class="form-control col-sm-3" name="action">
                                                <option value="approve">Approve</option>
                                                <option value="delete">Delete</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-md">Action</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop
