<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			Administration
			@show
		</title>
		<meta name="keywords" content="your, awesome, keywords, here" />
		<meta name="author" content="Jon Doe" />
		<meta name="description" content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei." />

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- CSS
		================================================== -->
		@section('styles-include')
			{{ Basset::show('public.css') }}
		@show

		<style>
			@section('styles')
				body {
					padding-top: 60px;
				}
			@show
		</style>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Favicons
		================================================== -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">
		<link rel="shortcut icon" href="{{ asset('assets/ico/favicon.png') }}">
	</head>

	<body>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		  	<!-- Brand and toggle get grouped for better mobile display -->
		  	<div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			      	<span class="sr-only">Toggle navigation</span>
			      	<span class="icon-bar"></span>
			      	<span class="icon-bar"></span>
			      	<span class="icon-bar"></span>
			    </button>
		    	<a class="navbar-brand" href="#">Administration</a>
		  	</div>
		  	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav">
					<li{{ (Request::is('admin') ? ' class="active"' : '') }}><a href="{{ URL::to('admin') }}"><i class="icon-home icon-white"></i> Home</a></li>
					<li class="dropdown{{ (Request::is('admin/blogs*') ? ' active' : '') }}">
						<a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/blogs') }}">
							<i class="icon-user icon-white"></i> Blogs <span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li{{ (Request::is('admin/blogs') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/blogs') }}"><i class="icon-user"></i> Blogs</a></li>
							<li{{ (Request::is('admin/blogs/categories') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/blogs/categories') }}"><i class="icon-user"></i> Categories</a></li>
						</ul>
					</li>
					<li{{ (Request::is('admin/categories*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/categories') }}"><i class="icon-list-alt icon-white"></i> Categories</a></li>
					<li class="dropdown{{ (Request::is('admin/users*|admin/groups*') ? ' active' : '') }}">
						<a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/users') }}">
							<i class="icon-user icon-white"></i> Users <span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/users') }}"><i class="icon-user"></i> Users</a></li>
							<li{{ (Request::is('admin/groups*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/groups') }}"><i class="icon-user"></i> Groups</a></li>
						</ul>
					</li>
					<li{{ (Request::is('admin/logins*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/logins') }}"><i class="icon-th-list icon-white"></i> Login activity</a></li>
					<li{{ (Request::is('admin/app/requests') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/app/requests') }}"><i class="icon-th-list icon-white"></i> App requests</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ URL::to('/') }}">View Homepage</a></li>
					<li class="divider-vertical"></li>
					<li><a href="{{ route('logout') }}">Logout</a></li>
				</ul>

		  	</div><!-- /.navbar-collapse -->
		</nav>
		<div class="container">

			<!-- Notifications -->
			@include('frontend/notifications')

			<!-- Content -->
			@yield('content')
		</div>

		@section('scripts')
			{{ Basset::show('public.js') }}
	    @show

	</body>
</html>
