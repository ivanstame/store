@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
	@parent · Questions managment
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
	<h3>
		Questions managment

		<div class="pull-right">
			<a href="{{ URL::to('admin/questions/create') }}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Create question</a>
		</div>
	</h3>
</div>

<a class="btn btn-medium" href="{{ URL::to('admin/questions?withTrashed=true') }}">Include Deleted Questions</a>
<a class="btn btn-medium" href="{{ URL::to('admin/questions?onlyTrashed=true') }}">Include Only Deleted Questions</a>

{{ $questions->links() }}
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th class="span1">Id</th>
				<th class="span2">Question</th>
				<th class="span2">Answer</th>
				<th class="span1">Status</th>
				<th class="span1">Answered on</th>
				<th class="span1">Failed on</th>
				<th class="span1">Created</th>
				<th class="span2">Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($questions as $q)
			<tr>
				<td>{{ $q->id }}</td>
				<td>{{ $q->question }}</td>
				<td>{{ $q->answer }}</td>
				<td>{{ $q->active == true ? 'Active' : 'Inactive' }}</td>
				<td>{{ $q->answered }}</td>
				<td>{{ $q->failed }}</td>
				<td>{{ $q->created_at() }}</td>
				<td>
					<a href="{{ route('update/question', $q->id) }}" class="btn btn-mini">@lang('button.edit')</a>
					@if ( ! is_null($q->deleted_at))
						<a href="{{ route('restore/question', $q->id) }}" class="btn btn-mini btn-warning">@lang('button.restore')</a>
					@else
						<a href="{{ route('delete/question', $q->id) }}" class="btn btn-mini btn-danger">@lang('button.delete')</a>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
{{ $questions->links() }}
@stop
