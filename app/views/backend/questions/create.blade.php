@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
	@parent
	· Question creation
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Create a security question

			<div class="pull-right">
				<a href="{{ route('admin/questions') }}" class="btn btn-small btn-info"><i class="icon-circle-arrow-left icon-white"></i> Go back</a>
			</div>
		</h3>
	</div>
	<form class="form-horizontal" method="POST" action="{{ URL::to('admin/questions/create') }}">
		{{ Form::token() }}

		<div class="{{ $errors->has('question') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="question">Question</label>
			<div class="controls">
				<input class="input-xlarge" type="text" id="question" placeholder="Question" name="question" value="{{ Input::old('question') }}">
				{{ $errors->first('question', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

		<div class="{{ $errors->has('answer') ? 'control-group error' : 'control-group' }}">
			<label class="control-label" for="answer">Answer</label>
			<div class="controls">
				<input class="input-medium" type="text" id="answer" placeholder="Answer for question" name="answer" value="{{ Input::old('answer') }}">
				{{ $errors->first('answer', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
			<label class="checkbox">
				<input type="checkbox" name="active" id="active" value="1" checked/> Activate now
			</label>
			</div>
		</div>

	  	<div class="control-group">
			<div class="controls">
		      	<button type="submit" class="btn btn-success">Create</button>
		      	<button type="reset" class="btn btn-inverse">Reset</button>
		    </div>
	  	</div>
	</form>
@stop
