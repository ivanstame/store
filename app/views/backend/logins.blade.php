@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
    @parent · Authentication records
@stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            Logging in activity
        </h3>
    </div>

    {{ $records->links() }}

    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th class="span1">ID</th>
                <th class="span1">User ID</th>
                <th class="span2">User IP</th>
                <th class="span2">Username</th>
                <th class="span2">User Email</th>
                <th class="span2">Login country</th>
                <th class="span2">Date</th>
            </tr>
        </thead>
        <tbody>
            @if ($records->count() >= 1)
                @foreach ($records as $record)
                <tr>
                    <td>{{ $record->id }}</td>
                    <td>{{ $record->user_id }}</td>
                    <td>{{ $record->login_ip }}</td>
                    <td>{{ $record->user->username }}</td>
                    <td>{{ $record->user->email }}</td>
                    <td>{{ $record->login_country}}</td>
                    <td>{{ $record->created_at() }}</td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5">No results</td>
                </tr>
            @endif
        </tbody>
    </table>

    {{ $records->links() }}
@stop