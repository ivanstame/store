<div class="appbox">
    <label class="applabel">
        <a href="{{ url('app/'. $app->slug) }}">{{ $app->name }}</a>
        @if($app->price > 0)
            <span class="label label-important pull-right">{{ '$' . number_format($app->price, 2, '.', ',') }}</span>
        @else
            <span  class="label label-success pull-right">Free</span>
        @endif
	</label>
	<a href="{{ url('app/'. $app->slug) }}"><img src="{{ asset($app->icon) }}" alt="" style="float: left; margin-right: 15px; max-width: 64px; max-height: 64px;" ></a>
	<p style="min-height: 60px;" >{{ $app->short_info }}</p>
    <div style="margin-top: 10px; clear: left;">
        @if($app->price > 0)
            <a href="{{ url('app/'. $app->slug) }}" class="btn btn-small btn-success">Buy</a>
        @else
            <a href="{{ url('app/'. $app->slug) }}" class="btn btn-small btn-primary">Download</a>
        @endif
        <a href="{{ $app->website }}" target="_blank" class="btn btn-small btn-default">Website</a>
    </div>
</div>
