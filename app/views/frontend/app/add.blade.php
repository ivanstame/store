@extends('frontend/layouts/default')

@section('title')
    @parent · Submit your application
@stop

@section('styles-include')
    @parent
    {{ Basset::show('wysiwyg.css') }}
@stop

@section('content')
    <div class="row">
        <h4>Enter details of your application</h4>
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <p><strong>Important!</strong> All fields except tags and silent argument are required in order to submit the form!</p>
        </div>
        <form method="POST" action="" accept-charset="UTF-8" enctype="multipart/form-data" role="form" class="form-horizontal"  onsubmit="return postForm()">
            {{ Form::token() }}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#step0" data-toggle="tab"><span class="badge pull-ledt">0</span>&nbsp;Category</a></li>
                <li><a href="#step1" data-toggle="tab"><span class="badge pull-ledt">1</span>&nbsp;Application info</a></li>
                <li><a href="#step2" data-toggle="tab"><span class="badge pull-ledt">2</span>&nbsp;Version info</a></li>
                <li><a href="#step3" data-toggle="tab"><span class="badge pull-ledt">3</span>&nbsp;Developer info</a></li>
                <li><a href="#step4" data-toggle="tab"><span class="badge pull-ledt">4</span>&nbsp;Finish</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="step0">
                	<div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="category">Select category for your app</label>
                                <div class="col-sm-4">
                                    <select style="width: 250px;" name="category" id="category">
                                        @foreach(Category::where('top_level', true)->get() as $cat)
                                            {{ MenuHelper::makeSelectCategories($cat) }}
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="alert alert-info">
                                <p><strong>Note!</strong> If there is not suitable category for your application, select one of the corresponding operating systems.</p>
                            </div>
                        </div>
                	</div>
                </div>
                <div class="tab-pane" id="step1">
                	<div class="row">
                        <div class="form-group{{ $errors->first('name', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="name">Application Name</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="name" id="name" value="{{ Input::old('name') }}" required />
                                {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('slug', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="slug">Slug</label>
                            <div class="input-group col-sm-5">
                                <span class="input-group-addon">{{ str_finish(URL::to('/app'), '/') }}</span>
                                <input name="slug" id="slug" type="text" class="form-control" placeholder="Slug for your app" required value="{{ Input::old('slug') }}" >
                                {{ $errors->first('slug', '<span class="help-block">:message</span>') }}
                            </div>
                            <button id="generate" type="button" class="btn btn-default">Generate slug</button>
                        </div>

                        <div class="form-group{{ $errors->first('short_info', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="short_info">Short Info</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="short_info" id="short_info" value="{{ Input::old('short_info') }}" maxlength="255" required />
                                {{ $errors->first('short_info', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('long_info', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="long_info">Long info</label>
                            <div class="col-sm-6">
                                <textarea id="long_info" name="long_info" data-provide="markdown" rows="10" name="long_info">{{ Input::old('long_info') }}</textarea>
                                {{ $errors->first('long_info', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('icon', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="icon">Icon</label>
                            <div class="col-sm-5">
                                <input type="file" name="icon" required />
                                {{ $errors->first('icon', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('need_key', ' has-error') }}">
                            <label class="col-sm-offset-2 checkbox">
                                <input name="need_key" type="checkbox" value="need"> Is your app using serial keys for activation
                            </label>
                            {{ $errors->first('need_key', '<span class="help-block">:message</span>') }}
                        </div>

                        <div class="form-group{{ $errors->first('price', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="price">Price</label>
                            <div class="col-sm-5">
                                <input id="price" type="text" name="price" class="form-control" value="{{ Input::old('price') != null ? Input::old('price') : '' }}" placeholder="Price of your app or 0.00 for free" required >
                                {{ $errors->first('price', '<span class="help-block">:message</span>') }}
                            </div>
                            <button id="make_free" type="button" class="btn btn-default">Make free</button>
                        </div>

                        <div class="form-group{{ $errors->first('homepage', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="homepage">Homepage</label>
                            <div class="col-sm-5">
                                <input id="homepage" type="url" name="homepage" class="form-control" value="<?php if(Input::old('homepage')) echo Input::old('homepage'); else echo 'http://'; ?>" required >
                                {{ $errors->first('homepage', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('tags', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="tags">Tags  (optional)</label>
                            <div class="col-sm-3">
                                <input type="text" name="tags" placeholder="Add tags to make better in search results" class="form-control tm-input" />
                                {{ $errors->first('tags', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                	</div>
                </div>
                <div class="tab-pane" id="step2">
                	<div class="row">
                        <div class="form-group{{ $errors->first('version', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="version">Version</label>
                            <div class="col-sm-3">
                                <input class="form-control" type="text" name="version" id="version" value="{{ Input::old('version') }}" required />
                                {{ $errors->first('version', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('architecture', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="version">Architecture</label>
                            <div class="col-sm-6">
                                <label>
                                    <input type="radio" name="architecture" id="architecture" value="32" checked>
                                    x86 (32-bit)
                                </label>
                                &nbsp;
                                <label>
                                    <input type="radio" name="architecture" id="architecture" value="64">
                                    x64 (64-bit)
                                </label>
                                {{ $errors->first('architecture', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('silence', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="silence">Silent argument (optional)</label>
                            <div class="col-sm-3">
                                <input class="form-control" type="text" name="silence" id="silence" value="{{ Input::old('silence') }}" placeholder="Installer silence argument" />
                                {{ $errors->first('silence', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('changelog', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="changelog">Changelog</label>
                            <div class="col-sm-6">
                                <textarea id="changelog" name="changelog" data-provide="markdown" rows="10" name="changelog">{{ Input::old('changelog') }}</textarea>
                                {{ $errors->first('changelog', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('licence', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="licence">Licence</label>
                            <div class="col-sm-3">
                                <input class="form-control" type="text" name="licence" id="licence" value="{{ Input::old('licence') }}" placeholder="Version licence like BSD, MIT..." required />
                                {{ $errors->first('licence', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->first('installer', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="installer">Executable file (installer)</label>
                            <div class="col-sm-5">
                                <input type="file" name="installer" required />
                                {{ $errors->first('installer', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                	</div>
                </div>
                <div class="tab-pane" id="step3">
                	<div class="row">
                        <div class="form-group{{ $errors->first('developer', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="developer">Developer</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="developer" id="developer" value="{{ Input::old('name') }}" placeholder="Company or developer name" required />
                                {{ $errors->first('developer', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                         <div class="form-group{{ $errors->first('developer_email', ' has-error') }}">
                            <label class="col-sm-2 control-label" for="developer_email">Developer email</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="email" name="developer_email" id="developer_email" value="{{ Input::old('developer_email') }}" placeholder="Enter developer email address (very important)" required />
                                {{ $errors->first('developer_email', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                	</div>
                </div>
                <div class="tab-pane" id="step4">
                	<div class="row">
                        <div class="{{ $errors->has('answer') ? 'form-group has-error' : 'form-group' }}">
                            <label class="col-sm-2 control-label" for="answer">Security question</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="answer" id="answer" placeholder="{{ html_entity_decode($question->question) }}" required >
                                <input type="hidden" name="qid" value="{{ $question->id }}" />
                                {{ $errors->first('answer', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>

                        <hr>

                        <!-- Form actions -->
                        <div class="form-group">
                            <div class="col-sm-4">
                                <a class="btn" href="{{ route('home') }}">Cancel</a>

                                <button type="submit" class="btn btn-primary">Submit for approval</button>
                            </div>
                        </div>
                	</div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('scripts')
    @parent
    {{ Basset::show('wysiwyg.js') }}
    <script type="text/javascript">

        $( document ).ready(function() {
            $("#category").select2({ minimumResultsForSearch: 20 });
            $('#long_info').summernote({
                height: "300px",
                toolbar: [
                    ['style', ['style']], // no style button
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    //['height', ['height']],
                    ['insert', ['picture', 'link']], // no insert buttons
                    //['table', ['table']], // no table button
                    //['help', ['help']] //no help button
                ]
            });

            $('#changelog').summernote({
                height: "200px",
                toolbar: [
                    ['style', ['style']], // no style button
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    //['height', ['height']],
                    ['insert', ['picture', 'link']], // no insert buttons
                    //['table', ['table']], // no table button
                    //['help', ['help']] //no help button
                ]
            });
            $(".tm-input").tagsManager({
                 prefilled: "{{ Input::old('tags') }}",
            });
        });

        var postForm = function() {
            var long_info = $('textarea[name="long_info"]').val($('#long_info').code());
            var changelog = $('textarea[name="changelog"]').val($('#changelog').code());
        }

        var slugChanged = false;

        $( "#name" ).keyup(function() {
            if(!slugChanged)
                $("#slug").val(generateSlug($("#name").val()));
        });

        $( "#slug" ).keyup(function() {
            slugChanged = true;
        });

        $( "#generate" ).click(function() {
            if($("#name").val() != "")
                $("#slug").val(generateSlug($("#name").val()));
        });

        $( "#make_free" ).click(function() {
            $("#price").val("0.00");
        });

        function generateSlug (value) {
          // 1) convert to lowercase
          // 2) remove dashes and pluses
          // 3) replace spaces with dashes
          // 4) remove everything but alphanumeric characters and dashes
          return value.toLowerCase().replace(/^\s+|\s+$/g, "").replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
        };
    </script>
@stop
