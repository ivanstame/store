@extends('frontend/layouts/default')

@section('title')
	Download {{ $app->name }}
@stop

@section('meta_title')
	<meta name="title" content="{{ $app->name }}" />
@stop

@section('meta_description')
	<meta name="description" content="{{ $app->short_info }}" />
@stop

@section('meta_keywords')
	<meta name="keywords" content="{{ $app->tags }}" />
@stop

@section('content')
<div id="main" class="row">
	<ul class="breadcrumb">
        {{ $app->getCrumbs() }}
    </ul>

    <div id="left" class="col-md-9">
    	<div class="row" style="min-height: 84px;">
            <img src="{{ asset($app->icon) }}" alt="" style="float: left; margin-right: 15px; max-width: 64px; max-height: 64px;">
            <h3>{{ $app->name }} <small>{{ $version->version }}</small></h3>
        </div>


		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#info" data-toggle="tab">Info</a></li>
			<li><a href="#details" data-toggle="tab">Details</a></li>
			<li><a href="#media" data-toggle="tab">Media</a></li>
			<li><a href="#changelog" data-toggle="tab">Changelog</a></li>
			<li><a href="#bugs" data-toggle="tab">Bugs</a></li>
			<li><a href="#comments" data-toggle="tab">Comments</a></li>
            @if(Sentry::check())
                @if($app->isPurchased(Sentry::getId()))
                    <li><a href="#key" data-toggle="tab">Serial key</a></li>
                @endif
            @endif
		</ul>

		<div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="info">
                <div class="row">
                    <p>
                        {{ html_entity_decode($app->info) }}
                    </p>
                </div>
            </div>
            <div class="tab-pane fade" id="details">
                <div class="row">
                    <table class="table">
                        <tbody>
                            <tr>
                            	<td><b>Added by</b></td>
                                <td>
                                    <a href="{{ route('user-page', array($app->user->id)) }}">{{ $app->user->first_name . ' ' . $app->user->last_name }}</a>
                                </td>
                            </tr>
                            <tr>
                            	<td><b>Name</b></td>
                                <td>
                                    {{ $app->name }}
                                </td>
                            </tr>
                            <tr>
                            	<td><b>Version</b></td>
                                <td>
                                    {{ $version->version }}
                                </td>
                            </tr>
                            <tr>
                            	<td><b>Filename</b></td>
                                <td>
                                    {{ $version->filename }}
                                </td>
                            </tr>
                            <tr>
                            	<td><b>Developer</b></td>
                                <td>
                                    {{ $app->developer }}
                                </td>
                            </tr>
                            <tr>
                            	<td><b>Licence</b></td>
                                <td>
                                    {{ $version->licence }}
                                </td>
                            </tr>
                            <tr>
                            	<td><b>Date Added</b></td>
                                <td>
                                    {{ $version->created_at() }}
                                </td>
                            </tr>
                            <tr>
                            	<td><b>MD5</b></td>
                                <td>
                                    {{ $version->md5 }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="media">
                <div class="row">
    				<p>Screenshots and videos</p>
                    @foreach ($app->screenshots as $image)
                        <a target="_blank" href="{{ asset('assets/app_screenshots/' . $image->filename) }}"><img src="{{ asset('assets/app_screenshots/' . $image->filename) }}" alt="" style="float: left; margin-right: 15px; margin-bottom: 15px; max-width: 200px; max-height: 160px;" class="img-thumbnail"></a>
                    @endforeach
                </div>
            </div>
			<div class="tab-pane fade" id="changelog">
                <div class="row">
                    <p>
                        {{ $version->changelog }}
                    </p>
                </div>
            </div>
            <div class="tab-pane fade" id="bugs">
                <div class="row">
                    @if(count($bugs) <= 0)
                        <p>There are no reported bugs!</p>
                    @endif
                    @foreach ($bugs as $bug)
                        <div class="appbox">
                            <label class="applabel">{{ $bug->caption }}<small> in {{ $bug->version->version }}</small></label>
                            <table class="table table table-condensed">
                				<thead>
                                    <tr>
                                        <th class="span1">Status</th>
                                        <th class="span2">Priority</th>
                                        <th class="span2">Reported</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if($bug->is_fixed)
                                                <span class="label label-success">Fixed</span>
                                            @else
                                                <span class="label label-important">Not fixed</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($bug->priority == 1)
                                                <span class="label label-secondary">Low</span>
                                            @endif
                                            @if($bug->priority == 2)
                                                <span class="label label-warning">Medium</span>
                                            @endif
                                            @if($bug->priority == 3)
                                                <span class="label label-important">High</span>
                                            @endif
                                        </td>
                                        <td>{{ $bug->created_at() }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p>
                            <dl>
                                <dd>{{ $bug->body }}</dd>
                            </dl>
                            </p>
                        </div>
                    @endforeach
                </div>
			</div>
            <div class="tab-pane fade" id="comments">
                <div class="row">
                    @foreach ($app->commentsWhere() as $comment)
                        {{ MenuHelper::makeComments($comment) }}
                    @endforeach

                    @if ( ! Sentry::check())
                        You need to be logged in to add comments.<br /><br />
                        Click <a href="{{ route('signin') }}">here</a> to login into your account.
                        @else
                        <h4>Add a comment</h4>
                        <form method="post" action="{{ route('comment-app', $app->slug) }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <div class="form-group{{ $errors->first('comment', ' error') }}">
                                <div class="col-sm-6">
                                    <textarea class="col-sm-12 form-control" rows="4" name="comment" id="comment">{{ Input::old('comment') }}</textarea>
                                    {{ $errors->first('comment', '<span class="help-inline">:message</span>') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4">
                                    <input type="submit" class="btn btn-primary" id="submit" value="Submit" />
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
            @if(Sentry::check())
                @if($app->isPurchased(Sentry::getId()))
                    <div class="tab-pane fade" id="key">
                        <div class="row">
                            <p>Your serial key is:</p>
                            <h5>{{ $app->getSerialKey(Sentry::getId()) }}</h5>
                        </div>
                    </div>
                @endif
            @endif
        </div>
	</div>

	<div id="right" class="col-md-3">
		@if(Sentry::check())
			@if($app->isFree())
                <a href="{{ route('download-app', array($app->slug, $version->id)) }}" class="btn btn-large btn-block btn-primary">Download ({{ Helpers::bytesToSize($version->size, 1) }})</a>
			@else
                @if($app->isPurchased(Sentry::getId()))
                    <a href="{{ route('download-app', array($app->slug, $version->id)) }}" class="btn btn-large btn-block btn-primary">Download ({{ Helpers::bytesToSize($version->size, 1) }})</a>
                @else
                    @if($app->price > Sentry::getUser()->balance)
                        <a href="#" id="noMoney" data-toggle="tooltip" title="You don't have enough money to buy it." class="btn btn-large btn-block btn-success disabled">Buy (${{ number_format($app->price, 2, '.', ',') }}) </a>
                    @else
                        @if($app->haveKeysAvailable())
                            <a href="{{ route('buy-app', $app->slug) }}" class="btn btn-large btn-block btn-success">Buy (${{ number_format($app->price, 2, '.', ',') }}) </a>
                        @else
                            <a href="#" id="noKeys" data-toggle="tooltip" title="There are no avaliable keys." class="btn btn-large btn-block btn-success disabled">Buy (${{ number_format($app->price, 2, '.', ',') }}) </a>
                        @endif
                    @endif
                @endif
            @endif
		@else
			@if($app->isFree())
				<a href="{{ route('download-app', array($app->slug, $version->id)) }}" class="btn btn-large btn-block btn-primary">Download ({{ Helpers::bytesToSize($version->size, 1) }})</a>
			@else
				<a href="{{ route('buy-app', array($app->slug)) }}" class="btn btn-large btn-block btn-primary" >Login to buy</a>
            @endif
		@endif

		<center>
			<span class="label label-success">selected {{ $version->version }}</span>
        	<span class="label label-info">latest {{ Version::where('id', $app->version_id)->first()->version }}</span>


            <h5>Other versions</h5>
            @foreach(Version::where('app_id', '=', $app->id)->get() as $ver)
                @if($version->version == $ver->version)
                    @if(Sentry::getId() == $app->user_id || Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
                        <span>{{ $app->name }} {{ $ver->version }}</span> (<a href="{{ route('edit-version', array($app->slug, $ver->id)) }}">Edit</a>) (<a class="confirm" href="{{ route('delete-version', array($app->slug, $ver->id)) }}">Delete</a>)<br/>
                    @else
                        <span>{{ $app->name }} {{ $ver->version }}</span><br/>
                    @endif
                @else
                    @if(Sentry::getId() == $app->user_id || Sentry::getUser()->hasAnyAccess(array('owner', 'admin')))
                        <span><a href="{{ url('/app/' . $app->slug . '/' . $ver->id) }}">{{ $app->name }} {{ $ver->version }}</a></span> (<a href="{{ route('edit-version', array($app->slug, $ver->id)) }}">Edit</a>) (<a class="confirm" href="{{ route('delete-version', array($app->slug, $ver->id)) }}">Delete</a>)<br/>
                    @else
                        <span><a href="{{ url('/app/' . $app->slug . '/' . $ver->id) }}">{{ $app->name }} {{ $ver->version }}</a></span><br/>
                    @endif
                @endif
            @endforeach
        </center>

	</div>
</div>
@stop

@section('scripts')
    @parent
    <script type="text/javascript">
        $("#noMoney").tooltip();
        $("#noKeys").tooltip();
    </script>

    <script type="text/javascript">
        var hash = window.location.hash,
        hashPart=hash.split('!')[1],
        activeTab=$('ul.nav a[href="#' + hashPart + '"]');
        activeTab && activeTab.tab('show');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            window.location.hash = '#!'+$(this).attr('href').split('#')[1];
        });

        $('.confirm').on('click', function () {
            return confirm('Are you sure you want to delete this version?');
        });

    </script>
@stop
<?php $app->addView(); ?>
