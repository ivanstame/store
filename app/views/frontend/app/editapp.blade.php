@extends('frontend/layouts/default')

@section('title')
    @parent · Edit application
@stop

@section('styles-include')
    @parent
    {{ Basset::show('wysiwyg.css') }}
@stop

@section('content')
    <div class="page-header">
        <h3>Editing {{ $app->name }}</h3>
    </div>
    <div class="row">
        <form method="POST" action="" accept-charset="UTF-8" role="form" class="form-horizontal"  onsubmit="return postForm()">
            {{ Form::token() }}

            <div class="row">
                <div class="form-group{{ $errors->first('name', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="name">Application Name</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="name" id="name" value="{{ $app->name }}" required />
                        {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('slug', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="slug">Slug</label>
                    <div class="input-group col-sm-5">
                        <span class="input-group-addon">{{ str_finish(URL::to('/app'), '/') }}</span>
                        <input name="slug" id="slug" type="text" class="form-control" placeholder="Slug for your app" required value="{{ $app->slug }}" >
                        {{ $errors->first('slug', '<span class="help-block">:message</span>') }}
                    </div>
                    <button id="generate" type="button" class="btn btn-default">Generate slug</button>
                </div>

                <div class="form-group{{ $errors->first('short_info', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="short_info">Short Info</label>
                    <div class="col-sm-5">
                        <input class="form-control" type="text" name="short_info" id="short_info" value="{{ $app->short_info }}" maxlength="255" required />
                        {{ $errors->first('short_info', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('long_info', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="long_info">Long info</label>
                    <div class="col-sm-6">
                        <textarea id="long_info" name="long_info" data-provide="markdown" rows="10" name="long_info">{{ $app->info }}</textarea>
                        {{ $errors->first('long_info', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('need_key', ' has-error') }}">
                    <label class="col-sm-offset-2 checkbox">
                        <input name="need_key" type="checkbox" value="need" {{ $app->need_key ? 'checked' : '' }}> Is your app using serial keys for activation
                    </label>
                    {{ $errors->first('need_key', '<span class="help-block">:message</span>') }}
                </div>

                <div class="form-group{{ $errors->first('price', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="price">Price</label>
                    <div class="col-sm-5">
                        <input id="price" type="text" name="price" class="form-control" value="{{ $app->price }}" placeholder="Price of your app or 0.00 for free" required >
                        {{ $errors->first('price', '<span class="help-block">:message</span>') }}
                    </div>
                    <button id="make_free" type="button" class="btn btn-default">Make free</button>
                </div>

                <div class="form-group{{ $errors->first('homepage', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="homepage">Homepage</label>
                    <div class="col-sm-5">
                        <input id="homepage" type="url" name="homepage" class="form-control" value="{{ $app->website }}" required >
                        {{ $errors->first('homepage', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('tags', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="tags">Tags  (optional)</label>
                    <div class="col-sm-3">
                        <input type="text" name="tags" placeholder="Add tags to make better in search results" class="form-control tm-input" value="{{ Input::old('tags') }}"/>
                        {{ $errors->first('tags', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('developer', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="developer">Developer</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="developer" id="developer" value="{{ $app->developer }}" placeholder="Company or developer name" required />
                        {{ $errors->first('developer', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                 <div class="form-group{{ $errors->first('developer_email', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="developer_email">Developer email</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="email" name="developer_email" id="developer_email" value="{{ $app->dev_email }}" placeholder="Enter developer email address (very important)" required />
                        {{ $errors->first('developer_email', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-7">
                        <a class="btn btn-default" href="{{ route('home') }}">Cancel</a>

                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('scripts')
    @parent
    {{ Basset::show('wysiwyg.js') }}
    <script type="text/javascript">

        $( document ).ready(function() {
            $('#long_info').summernote({
                height: "200px",
                toolbar: [
                    ['style', ['style']], // no style button
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    //['height', ['height']],
                    ['insert', ['picture', 'link']], // no insert buttons
                    //['table', ['table']], // no table button
                    //['help', ['help']] //no help button
                ]
            });

            $(".tm-input").tagsManager({
                 prefilled: "{{ $app->tags }}",
            });
        });

        var slugChanged = false;

        $( "#name" ).keyup(function() {
            if(!slugChanged)
                $("#slug").val(generateSlug($("#name").val()));
        });

        $( "#slug" ).keyup(function() {
            slugChanged = true;
        });

        $( "#generate" ).click(function() {
            if($("#name").val() != "")
                $("#slug").val(generateSlug($("#name").val()));
        });

        $( "#make_free" ).click(function() {
            $("#price").val("0.00");
        });

        var postForm = function() {
            var long_info = $('textarea[name="long_info"]').val($('#long_info').code());
        }

        function generateSlug (value) {
          // 1) convert to lowercase
          // 2) remove dashes and pluses
          // 3) replace spaces with dashes
          // 4) remove everything but alphanumeric characters and dashes
          return value.toLowerCase().replace(/^\s+|\s+$/g, "").replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
        };
    </script>
@stop
