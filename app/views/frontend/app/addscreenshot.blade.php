@extends('frontend/layouts/default')

@section('title')
    @parent · Add screenshot
@stop

@section('content')
    <div class="page-header">
        <h3>Add scrrenshot to your app</h3>
    </div>
    <div class="row">
        <form method="POST" action="" accept-charset="UTF-8" enctype="multipart/form-data" role="form" class="form-horizontal"  onsubmit="return postForm()">
            {{ Form::token() }}

            <div class="row">
                <div class="form-group{{ $errors->first('caption', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="caption">Caption</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="caption" id="caption" value="{{ Input::old('caption') }}" required />
                        {{ $errors->first('caption', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('image', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="image">Image file</label>
                    <div class="col-sm-5">
                        <input type="file" name="image" required />
                        {{ $errors->first('image', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-7">
                        <a class="btn btn-default" href="{{ route('home') }}">Cancel</a>

                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
