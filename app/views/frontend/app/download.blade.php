@extends('frontend/layouts/default')

@section('title')
	@parent · Confirm purchase
@stop

@section('content')
	<div class="row">
		<div class="span8 offset2">
			<h3 style="margin-bottom: 0px;" >Downloading {{ $app->name }}...</h3>
			<p>Thank you. Your download will start automatically in <span id="count">5</span>. If not or you just can't wait <a id="link" href="{{ $downloadString }}">click here.</a></p>
		</div>
	</div>
@stop

@section('scripts')
	@parent

	<script type="text/javascript">
		$( document ).ready(function() {
		    var counter = 5;
		    var clicked = false;
		    var running = true;

		    $("#link").click(function(e) {
  				clicked = true;
			});

			setInterval(function() {
				if(running) {
					if(!clicked) {
				    	counter--;
				    	if(counter < 0) {
				        	window.location = "{{ $downloadString }}";
				        	running = false;
				    	} else {
				        	document.getElementById("count").innerHTML = counter;
				        }
				    }
				}
			}, 1000);
		});
	</script>
@stop
