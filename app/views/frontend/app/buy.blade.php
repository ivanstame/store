@extends('frontend/layouts/default')

@section('title')
	@parent · Confirm purchase
@stop

@section('content')
	<div class="row">
	    <div class="span8">
	        <h4>Are you sure you want to buy {{ $app->name }}?</h4>
	        <form style="margin-bottom: 0px;" action="{{ url('app/' . $app->slug . '/buy') }}" method="POST" >

	        	{{ Form::token() }}

	            <input type="hidden" name="appslug" value="{{ $app->slug }}" />
	            <button type="submit" id="confirm" class="button primary" style="margin-bottom: 10px" >Confirm (${{ number_format($app->price, 2, '.', ',') }})</button>
	        </form>
	    </div>
	</div>
@stop
