@extends('frontend/layouts/default')

@section('title')
	@parent · Submit your application
@stop

@section('content')
	<div class="row">
	    <div class="span8">
	        <h4>Choose in what top category your application should be</h4>
	        <a class="btn btn-success">Windows</a>
            <a class="btn btn-danger">Mac</a>
            <a class="btn btn-primary">Linux</a>
  	    </div>
	</div>
@stop

@section('scripts')
	@parent
@stop
