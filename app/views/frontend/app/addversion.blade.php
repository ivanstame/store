@extends('frontend/layouts/default')

@section('title')
    @parent · Add new verison
@stop

@section('styles-include')
    @parent
    {{ Basset::show('wysiwyg.css') }}
@stop

@section('content')
    <div class="page-header">
        <h3>Add new version to {{ $app->name }}</h3>
    </div>
    <div class="row">
        <form method="POST" action="" accept-charset="UTF-8" enctype="multipart/form-data" role="form" class="form-horizontal"  onsubmit="return postForm()">
            {{ Form::token() }}

            <div class="row">
                <div class="form-group{{ $errors->first('version', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="version">Version</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="version" id="version" value="{{ Input::old('version') }}" required />
                        {{ $errors->first('version', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('architecture', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="version">Architecture</label>
                    <div class="col-sm-6">
                        <label>
                            <input type="radio" name="architecture" id="architecture" value="32" checked>
                            x86 (32-bit)
                        </label>
                        &nbsp;
                        <label>
                            <input type="radio" name="architecture" id="architecture" value="64">
                            x64 (64-bit)
                        </label>
                        {{ $errors->first('architecture', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="remember-me" name="main" value="1"> Set this version as current(latest) one.
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->first('silence', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="silence">Silent argument (optional)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="silence" id="silence" value="{{ Input::old('silence') }}" placeholder="Installer silence argument" />
                        {{ $errors->first('silence', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('changelog', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="changelog">Changelog</label>
                    <div class="col-sm-6">
                        <textarea id="changelog" name="changelog" data-provide="markdown" rows="10" name="changelog" value="{{ Input::old('changelog') }}" ></textarea>
                        {{ $errors->first('changelog', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('licence', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="licence">Licence</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="licence" id="licence" value="{{ Input::old('licence') }}" placeholder="Version licence like BSD, MIT..." required />
                        {{ $errors->first('licence', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <div class="form-group{{ $errors->first('installer', ' has-error') }}">
                    <label class="col-sm-2 control-label" for="installer">Executable file (installer)</label>
                    <div class="col-sm-5">
                        <input type="file" name="installer" required />
                        {{ $errors->first('installer', '<span class="help-block">:message</span>') }}
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-7">
                        <a class="btn btn-default" href="{{ route('home') }}">Cancel</a>

                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('scripts')
    @parent
    {{ Basset::show('wysiwyg.js') }}
    <script type="text/javascript">

        $( document ).ready(function() {
            $('#changelog').summernote({
                height: "200px",
                toolbar: [
                    ['style', ['style']], // no style button
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    //['height', ['height']],
                    ['insert', ['picture', 'link']], // no insert buttons
                    //['table', ['table']], // no table button
                    //['help', ['help']] //no help button
                ]
            });
        });

        var postForm = function() {
            var changelog = $('textarea[name="changelog"]').val($('#changelog').code());
        }
    </script>
@stop
