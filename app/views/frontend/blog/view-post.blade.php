@extends('frontend/layouts/default')

@section('title')
	@parent · {{ $post->title }}
@stop

@section('meta_title')
	{{ $post->meta_title }}
@stop

@section('meta_description')
	{{ $post->meta_description }}
@stop

@section('meta_keywords')
	{{ $post->meta_keywords }}
@stop

@section('content')
	<h3>{{ $post->title }} <small>in {{ $post->getCategory()->name }}</small></h3>

	<p>{{ html_entity_decode($post->content()) }}</p>

	<div>
		<span class="badge badge-info" title="{{ $post->created_at }}">Posted {{ $post->created_at->diffForHumans() }}</span>
	</div>

	<hr />

	<a id="comments"></a>
	<h4>{{ $comments->count() }} Comments</h4>

	@if ($comments->count())
		@foreach ($comments as $comment)
		<div class="media">
			<a class="pull-left" href="#">
				<img class="media-object" src="{{ $comment->author->gravatar() }}" alt="64x64" style="width: 64px; height: 64px;">
			</a>
			<div class="media-body">
                <h4 class="media-heading">{{ $comment->author->fullName() }} <small>{{ $comment->date() }}</small></h4>
                {{ $comment->content() }}
			</div>
        </div>


			<hr />
		@endforeach
	@else
		<hr />
	@endif

	@if ( ! Sentry::check())
		You need to be logged in to add comments.<br /><br />
		Click <a href="{{ route('signin') }}">here</a> to login into your account.
		@else
		<h4>Add a comment</h4>
		<form method="post" action="{{ route('view-post', $post->slug) }}">
			<!-- CSRF Token -->
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />

			<!-- Comment -->
			<div class="control-group{{ $errors->first('comment', ' error') }}">
				<textarea class="input-block-level" rows="4" name="comment" id="comment">{{ Input::old('comment') }}</textarea>
				{{ $errors->first('comment', '<span class="help-inline">:message</span>') }}
			</div>

			<!-- Form actions -->
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn" id="submit" value="Submit" />
				</div>
			</div>
		</form>
	@endif
@stop

<?php $post->addView(); ?>
