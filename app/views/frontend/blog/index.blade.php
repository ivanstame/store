@extends('frontend/layouts/default')

@section('title')
	@parent · Blog
@stop

@section('meta_title')
	<meta name="title" content="{{ Config::get('store.title') }}" />
@stop

@section('meta_description')
	<meta name="description" content="{{ Config::get('store.description') }}" />
@stop

@section('meta_keywords')
	<meta name="keywords" content="{{ Config::get('store.keywords') }}" />
@stop

@section('content')
	@foreach ($posts as $post)
		<div class="row">
			<div class="span8">

				<div class="row">
					<div class="span8">
						<h4><strong><a href="{{ $post->url() }}">{{ $post->title }}</a></strong><small> {{ $post->created_at() }}</small></h4>
					</div>
				</div>

				<div class="row">
					<div class="span2">
						<a href="{{ $post->url() }}" class="thumbnail"><img src="{{ $post->thumbnail() }}" alt=""></a>
					</div>
					<div class="span6">
						<p>
							{{ html_entity_decode(Str::limit($post->content, 200)) }}
						</p>
						<p><a class="btn btn-mini" href="{{ $post->url() }}">Read more...</a></p>
					</div>
				</div>

				<div class="row">
					<div class="span8">
						<p></p>
						<p>
							<i class=".glyphicon .glyphicon-user"></i> by <span class="muted">{{ $post->author->first_name }}</span>
							| <i class=".glyphicon .glyphicon-comment"></i> <a href="{{ $post->url() }}#comments">{{ $post->comments()->count() }} Comments</a>
						</p>
					</div>
				</div>
			</div>
		</div>

		<hr />
	@endforeach

	{{ $posts->links() }}
@stop
