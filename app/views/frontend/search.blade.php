@extends('frontend/layouts/default')

@section('title')
	Search results for {{ $query }}
@stop

@section('meta_title')

@stop

@section('meta_description')

@stop

@section('meta_keywords')

@stop

@section('content')
	<div class="row">
	    <h2>Search for "{{ $query }}"</h2>
	   	@if(count($apps) == 0)
	   		<p>No apps found for '{{ $query }}'!</p>
	   	@endif
	    <ul class="block-grid two-up">
	    	@foreach ($apps as $app)
				<li>
                    @include('frontend/app')
                </li>
			@endforeach
	    </ul>
	</div>
@stop
