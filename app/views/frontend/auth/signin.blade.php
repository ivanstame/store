@extends('frontend/layouts/default')

@section('title')
	@parent · Account sing in
@stop

@section('content')
<div class="page-header">
	<h3>Sign in into your account</h3>
</div>
<div class="row">
	<form method="post" action="{{ route('signin') }}" class="form-horizontal" role="form">

		{{ Form::token() }}

		<div class="form-group{{ $errors->first('email', ' has-error') }}">
			<label class="col-sm-2 control-label" for="email">Email</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="email" id="email" value="{{ Input::old('email') }}" />
				{{ $errors->first('email', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<div class="form-group{{ $errors->first('password', ' has-error') }}">
			<label class="col-sm-2 control-label" for="password">Password</label>
			<div class="col-sm-4">
				<input class="form-control" type="password" name="password" id="password" value="" />
				{{ $errors->first('password', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<div class="form-group">
	    	<div class="col-sm-offset-2 col-sm-10">
	      		<div class="checkbox">
			        <label>
			          	<input type="checkbox" name="remember-me" id="remember-me" value="1" checked> Remember me
			        </label>
	      		</div>
	    	</div>
	  	</div>

		<hr>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-7">
				<a class="btn btn-default" href="{{ route('home') }}">Cancel</a>

				<button type="submit" class="btn btn-primary">Sign in</button>

				<a href="{{ route('forgot-password') }}" class="btn btn-link">I forgot my password</a>
			</div>
		</div>
	</form>
</div>
@stop
