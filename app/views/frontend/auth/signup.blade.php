@extends('frontend/layouts/default')

@section('title')
	@parent · Account sign up
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>Sign up</h3>
</div>
<div class="row">
	<form method="post" action="{{ route('signup') }}" class="form-horizontal" autocomplete="off" role="form">
		<!-- CSRF Token -->
		{{ Form::token() }}

		<!-- First Name -->
		<div class="form-group{{ $errors->first('first_name', ' has-error') }} {{ $errors->first('last_name', ' has-error') }}">
			<label class="col-sm-2 control-label" for="first_name">First name</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="first_name" id="first_name" value="{{ Input::old('first_name') }}" placeholder="" required/>
				{{ $errors->first('first_name', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<div class="form-group{{ $errors->first('last_name', ' has-error') }}">
			<label class="col-sm-2 control-label" for="last_name">Last name</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="last_name" id="last_name" value="{{ Input::old('last_name') }}" placeholder="" required/>
				{{ $errors->first('last_name', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Username -->
		<div class="form-group{{ $errors->first('username', ' has-error') }}">
			<label class="col-sm-2 control-label" for="username">Username</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="username" id="username" value="{{ Input::old('username') }}" required/>
				{{ $errors->first('username', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Email -->
		<div class="form-group{{ $errors->first('email', ' has-error') }}">
			<label class="col-sm-2 control-label" for="email">Email</label>
			<div class="col-sm-4">
				<input class="form-control" type="email" name="email" id="email" value="{{ Input::old('email') }}" required/>
				{{ $errors->first('email', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Password -->
		<div class="form-group{{ $errors->first('password', ' has-error') }}">
			<label class="col-sm-2 control-label" for="password">Password</label>
			<div class="col-sm-4">
				<input class="form-control" type="password" name="password" id="password" value="" required/> <span>Your password is forceable in <span id="pwTime">less than one second.</span></span>
				{{ $errors->first('password', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Password Confirm -->
		<div class="form-group{{ $errors->first('password_confirm', ' has-error') }}">
			<label class="col-sm-2 control-label" for="password_confirm">Confirm Password</label>
			<div class="col-sm-4">
				<input class="form-control" type="password" name="password_confirm" id="password_confirm" value="" required/>
				{{ $errors->first('password_confirm', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<div class="{{ $errors->has('answer') ? 'form-group has-error' : 'form-group' }}">
			<label class="col-sm-2 control-label" for="answer">Security question</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="answer" class="span4" id="answer" placeholder="{{ html_entity_decode($question->question) }}" required>
				<input type="hidden" name="qid" value="{{ $question->id }}" />
				{{ $errors->first('answer', '<span class="help-block">:message</span>') }}
			</div>
        </div>

		<hr>

		<!-- Form actions -->
		<div class="form-group">
			<div class="col-sm-4">
				<a class="btn" href="{{ route('home') }}">Cancel</a>

				<button type="submit" class="btn btn-primary">Sign up</button>
			</div>
		</div>
	</form>
</div>
@stop

@section('scripts')
	@parent
	<script src="{{ asset('assets/js/jquery.pwdstr-1.0.source.js') }}"></script>
	<script type="text/javascript">
		$('#password').pwdstr('#pwTime');
	</script>
@stop
