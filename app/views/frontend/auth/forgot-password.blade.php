@extends('frontend/layouts/default')

@section('title')
	@parent · Forgot password
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>Forgot Password</h3>
</div>
<form method="post" action="" class="form-horizontal" role="role">
	
	{{ Form::token() }}

	<!-- Email -->
	<div class="form-group{{ $errors->first('email', ' has-error') }}">
		<label class="col-sm-2 control-label" for="email">Email</label>
		<div class="col-sm-6">
			<input class="form-control" type="text" name="email" id="email" value="{{ Input::old('email') }}" />
			{{ $errors->first('email', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<!-- Form actions -->
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-6">
			<a class="btn" href="{{ route('home') }}">Cancel</a>

			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</form>
@stop
