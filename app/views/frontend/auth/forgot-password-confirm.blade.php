@extends('frontend/layouts/default')

@section('title')
	@parent · Forgot password
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>Forgot Password</h3>
</div>
<form method="post" action="" class="form-horizontal">

	{{ Form::token() }}

	<div class="form-group{{ $errors->first('password', ' has-error') }}">
		<label class="col-sm-2 control-label" for="password">New Password</label>
		<div class="col-sm-4">
			<input class="form-control" type="password" name="password" id="password" value="{{ Input::old('password') }}" />
			{{ $errors->first('password', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group{{ $errors->first('password_confirm', ' has-error') }}">
		<label class="col-sm-2 control-label" for="password_confirm">Password Confirmation</label>
		<div class="col-sm-4">
			<input class="form-control" type="password" name="password_confirm" id="password_confirm" value="{{ Input::old('password_confirm') }}" />
			{{ $errors->first('password_confirm', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-4">
			<a class="btn btn-default" href="{{ route('home') }}">Cancel</a>

			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</form>
@stop
