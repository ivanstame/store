@extends('frontend/layouts/default')

@section('title')
	@parent · {{ Config::get('store.slogan') }}
@stop

@section('meta_title')
	<meta name="title" content="{{ Config::get('store.title') }}" />
@stop

@section('meta_description')
	<meta name="description" content="{{ Config::get('store.description') }}" />
@stop

@section('meta_keywords')
	<meta name="keywords" content="{{ Config::get('store.keywords') }}" />
@stop

@section('content')
	<div class="row">
		<div class="col-md-6">
			<h2>Popular apps</h2>
			@foreach ($popular as $app)
				@include('frontend/app')
			@endforeach
		</div>
		<div class="col-md-6">
			<h2>Most downloads</h2>
			@foreach ($downloaded as $app)
				@include('frontend/app')
			@endforeach
		</div>
	</div>
@stop
