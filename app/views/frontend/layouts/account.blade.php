@extends('frontend/layouts/default')

{{-- Page content --}}
@section('content')
	<div class="row">
		<div class="col-md-3">
			<ul class="nav nav-pills nav-stacked">
				<li{{ Request::is('account/profile') ? ' class="active"' : '' }}><a href="{{ URL::route('profile') }}">Profile</a></li>
				<li{{ Request::is('account/add-credit') ? ' class="active"' : '' }}><a href="{{ route('add-credit') }}">Add Credit</a></li>
				<li{{ Request::is('account/change-password') ? ' class="active"' : '' }}><a href="{{ URL::route('change-password') }}">Change Password</a></li>
				<li{{ Request::is('account/change-email') ? ' class="active"' : '' }}><a href="{{ URL::route('change-email') }}">Change Email</a></li>
				@if(!Sentry::getUser()->isDev())
					@if(!Sentry::getUser()->isAppliedFor())
						<li{{ Request::is('account/become-developer') ? ' class="active"' : '' }}><a href="{{ URL::route('apply-dev') }}">Become developer</a></li>
					@else
			  			<li disabled="disabled"><a href="#">Become Developer - <small>pending approval</small></a></li>
					@endif

				@else
					<li{{ Request::is('developer/apps') ? ' class="active"' : '' }}><a href="{{ URL::route('dev-apps') }}">My Apps</a></li>
				@endif
			</ul>
		</div>
		<div class="col-md-9">
			@yield('account-content')
		</div>
	</div>
@stop
