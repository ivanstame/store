<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8" />
		<title>
			@section('title')
			{{ Config::get('store.name') }}
			@show
		</title>

		@yield('meta_title')
		@yield('meta_description')
		@yield('meta_keywords')

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		@section('styles-include')
			{{ Basset::show('public.css') }}
		@show

		<style>
			@section('styles')
				body {
					padding-top: 60px;
				}
			@show
		</style>

		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">
		<link rel="shortcut icon" href="{{ asset('assets/ico/favicon.png') }}">
	</head>

	<body>
		<nav class="navbar navbar-masthead navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".bs-example-masthead-collapse-1">
			      	<span class="sr-only">Toggle navigation</span>
			      	<span class="icon-bar"></span>
			      	<span class="icon-bar"></span>
			      	<span class="icon-bar"></span>
			    </button>
			    <a class="navbar-brand" href="{{ route('home') }}">{{ Config::get('store.name') }} <small><span class="label label-info">alpha</span></small></a>
			</div>
	  		<div class="collapse navbar-collapse bs-example-masthead-collapse-1">

	    		<ul class="nav navbar-nav">
	                @foreach (Category::where('top_level', '=', 1)->get() as $menu)
	                    {{ MenuHelper::makeMenu($menu) }}
	                @endforeach
	    		</ul>

	    		<ul class="nav navbar-nav">
		            <li {{ (Request::is('blog*') ? 'class="active"' : '') }}><a href="{{ URL::to('blog') }}">Blog</a></li>
		    	</ul>

				<form class="navbar-form navbar-left" role="search" method="POST" action="{{ route('search_pre') }}">
			      	{{ Form::token() }}
			      	<div class="form-group">
			        	<input type="text" class="form-control" placeholder="Search" name="query">
			      	</div>
			      	<button type="submit" class="btn btn-primary">Search</button>
			    </form>

	            <ul class="nav navbar-nav navbar-right">
	                @if(Sentry::check())
	                    <li><a href="{{ route('profile') }}" class="">Your balance: ${{ number_format(Sentry::getUser()->balance, 2, '.', ',') }}</a></li>
	                    <li class="dropdown{{ (Request::is('account*') ? ' active' : '') }}">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Howdy {{ Sentry::getUser()->first_name }}<b class="caret"></b></a>
	                        <ul class="dropdown-menu">
	                            <li><a href="{{ route('profile') }}">My Profile</a></li>
								@if(!Sentry::getUser()->isDev())
									<li><a href="{{ route('apply-dev') }}">Become developer</a></li>
								@endif
	                            @if(Sentry::getUser()->isDev())
	                                <li class="divider"></li>
	                                <li><a href="{{ route('dev-apps') }}">My apps</a></li>
	                                <li><a href="{{ route('apply-app') }}">Submit Application ({{ Sentry::getUser()->project_limits == -1 ? '∞' : Sentry::getUser()->project_limits }})</a></li>
	                                <li><a href="#">Statistics (not available)</a></li>
	                                <li><a href="#">Withdraw (not available)</a></li>
	                            @endif
	                            @if(Sentry::getUser()->hasAnyAccess(array('admin', 'owner')))
	                                <li class="divider"></li>
	                                <li><a href="<?= Request::root() . '/admin' ?>" target="_blank">Admin Dashboard</a></li>
	                            @endif
	                            <li class="divider"></li>
	                            <li><a href="{{ route('logout') }}">Log out</a></li>
	                        </ul>
	                    </li>
	                @else
	                    <li {{ (Request::is('auth/signin') ? 'class="active"' : '') }}><a href="{{ route('signin') }}">Sign in</a></li>
	                    <li {{ (Request::is('auth/signup') ? 'class="active"' : '') }}><a href="{{ route('signup') }}">Sign up</a></li>
	                @endif
	            </ul>
	  		</div>
		</nav>

		<div class="container">
			@include('frontend/notifications')
			@yield('content')

			<hr />

			<footer>
				<p>
					<ul class="pull-right inline-list">
					  	<li><a href="{{ route('about-us') }}">About us</a></li>
					  	<li><a href="{{ route('contact-us') }}">Contact us</a></li>
					</ul>
					&copy; {{ Config::get('store.name') }} {{ date('Y') }}
				</p>
			</footer>
		</div>

		@section('scripts')
			{{ Basset::show('public.js') }}
	    @show

	</body>
</html>
