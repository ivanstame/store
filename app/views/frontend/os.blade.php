@extends('frontend/layouts/default')

@section('title')
    @parent · All apps in {{ $category->name }} category
@stop

@section('meta_title')

@stop

@section('meta_description')

@stop

@section('meta_keywords')

@stop

@section('content')
    <div class="row">
        @if($category->isParent())
            <h2>{{ $category->name }} applications</h2>
        @else
            <h2>{{ $category->name }} <small>{{ $category->getParent()->name }}</small></h2>
        @endif

        @if(count($apps) == 0)
            <p>There are no apps in the {{ $category->name }}!</p>
        @endif
        @if($hard)
            <ul class="block-grid two-up">
                @foreach ($apps as $app)
                    @if($app->category->getParent()->id == $category->id)
                        <li>
                            @include('frontend/app')
                        </li>
                    @endif
                @endforeach
            </ul>
        @else
            <ul class="block-grid two-up">
                @foreach ($apps as $app)
                    <li>
                        @include('frontend/app')
                    </li>
                @endforeach
            </ul>
        @endif
        {{ $apps->links() }}
    </div>
@stop
