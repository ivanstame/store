@extends('frontend/layouts/account')

@section('title')
    @parent · Add credit to your account
@stop

@section('account-content')
    <div class="page-header">
        <h4>Choose payment method</h4>
    </div>

    <ul class="nav nav-tabs">
        <li  class="active"><a href ="#paypal" data-toggle="tab">Paypal</a></li>
    </ul>

    <div class="tab-content col-sm-9">
        <div class="tab-pane fade in active" id="paypal">
            <div class="row">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="payPalForm" class="form-horizontal" role="form" autocomplete="off">
                    <input type="hidden" name="item_number" value="<?= Sentry::getId() ?>">
                    <input type="hidden" name="cmd" value="_xclick">
                    <input type="hidden" name="no_note" value="1">
                    <input type="hidden" name="business" value="info@zogitech.com">
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="hidden" name="return" value="{{    url('/getway/paypal/verify') }}">
                    <input name="item_name" type="hidden" id="item_name"  size="45" value="Add credit to: {{ Sentry::getUser()->username }}.">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="amount">Amount of money</label>
                        <div class="col-md-7">
                            <input name="amount" type="text" id="amount" class="form-control" size="45" placeholder="Enter amount you want to add.">
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <a class="btn" href="{{ route('home') }}">Cancel</a>

                            <button id="payButton" type="submit" class="btn btn-primary">Procede</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
