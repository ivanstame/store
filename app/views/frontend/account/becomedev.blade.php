@extends('frontend/layouts/account')

@section('title')
	@parent · Become a developer
@stop

@section('account-content')
	<div class="page-header">
		<h4>Apply for our developer program</h4>
	</div>
	<form method="post" action="" autocomplete="off" class="form-horizontal" role="form">
		{{ Form::token() }}

		<div class="form-group{{ $errors->first('idea', ' has-error') }}">
			<div class="col-md-7">
				<input class="form-control" type="text" name="idea" id="idea" value="{{ Input::old('idea') }}" placeholder="Tell us why you want to be in our dev program."/>
				{{ $errors->first('idea', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<hr>

		<div class="form-group">
			<div class="col-md-2">
				<button type="submit" class="btn btn-primary">Apply</button>
			</div>
		</div>
	</form>
@stop
