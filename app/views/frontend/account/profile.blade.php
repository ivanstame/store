@extends('frontend/layouts/account')

@section('title')
	@parent · Your profile
@stop

@section('account-content')
<div class="page-header">
	<h4>Update your Profile</h4>
</div>

<form method="post" action="" class="form-horizontal" autocomplete="off" role="form">
	
	{{ Form::token() }}

	<div class="form-group{{ $errors->first('first_name', ' has-error') }}">
		<label class="col-sm-2 control-label" for="first_name">First Name</label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="first_name" id="first_name" value="{{ Input::old('first_name', $user->first_name) }}" />
			{{ $errors->first('first_name', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group{{ $errors->first('last_name', ' has-error') }}">
		<label class="col-sm-2 control-label" for="last_name">Last Name</label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="last_name" id="last_name" value="{{ Input::old('last_name', $user->last_name) }}" />
			{{ $errors->first('last_name', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group{{ $errors->first('website', ' has-error') }}">
		<label class="col-sm-2 control-label" for="website">Website URL</label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="website" id="website" value="{{ Input::old('website', $user->website) }}" />
			{{ $errors->first('website', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group{{ $errors->first('country', ' has-error') }}">
		<label class="col-sm-2 control-label" for="country">Country</label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="country" id="country" value="{{ Input::old('country', $user->country) }}" />
			{{ $errors->first('country', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group{{ $errors->first('gravatar', ' has-error') }}">
		<label class="col-sm-2 control-label" for="gravatar">Gravatar Email <small>(Private)</small></label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="gravatar" id="gravatar" value="{{ Input::old('gravatar', $user->gravatar) }}" />
			{{ $errors->first('gravatar', '<span class="help-block">:message</span>') }}
		
			<p style="margin-top: 10px;">
				<img src="//secure.gravatar.com/avatar/{{ md5(strtolower(trim($user->gravatar))) }}" width="30" height="30" />
				<a href="http://gravatar.com">Change your avatar at Gravatar.com</a>.
			</p>
		</div>
	</div>

	<hr>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-4">
			<button type="submit" class="btn btn-primary">Update your Profile</button>
		</div>
	</div>
</form>
@stop
