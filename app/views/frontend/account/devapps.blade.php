@extends('frontend/layouts/account')

@section('title')
    My Apps - @parent
@stop

@section('account-content')
    <div class="page-header">
        <h3>
            My Apps

            <div class="pull-right">
                <a href="{{ route('apply-app') }}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Submit app</a>
            </div>
        </h3>
    </div>
    <div class="row">
        <div class="col-md-9">
            @if(count($apps) == 0)
                <p>You don't have any apps published.</p>
            @else
                @foreach($apps as $app)
                    <div class="appbox">
                        <label class="applabel">
                            <a href="{{ url('app/'. $app->slug) }}">{{ $app->name }}</a>
                            @if($app->price > 0)
                                <span class="label label-important pull-right">{{ '$' . number_format($app->price, 2, '.', ',') }}</span>
                            @else
                                <span  class="label label-success pull-right">Free</span>
                            @endif
                        </label>
                        <a href="{{ url('app/'. $app->slug) }}"><img src="{{ asset($app->icon) }}" alt="" style="float: left; margin-right: 15px; max-width: 64px; max-height: 64px;"></a>
                        <p style="min-height: 60px;" >{{ $app->short_info }}</p>
                        <div style="margin-top: 10px; margin-left: 79px; clear: left;">
                            <a href="{{ url('app/'. $app->slug . '/edit') }}" class="btn btn-small btn-primary">Edit</a>
                            <a href="{{ url('app/'. $app->slug . '/add/version') }}" class="btn btn-small btn-primary">Add new version</a>
                            <a href="{{ url('app/'. $app->slug . '/stats') }}" class="btn btn-small btn-success" disabled>View statistics</a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@stop
