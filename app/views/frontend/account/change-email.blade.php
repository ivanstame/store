@extends('frontend/layouts/account')

@section('title')
	@parent · Change your email
@stop

@section('account-content')
<div class="page-header">
	<h4>Change your Email</h4>
</div>

<form method="post" action="" class="form-horizontal" autocomplete="off">
	
	{{ Form::token() }}

	<input type="hidden" name="formType" value="change-email" />

	<div class="form-group{{ $errors->first('email', ' has-error') }}">
		<label class="col-sm-3 control-label" for="email">New Email</label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="email" id="email" value="" />
			{{ $errors->first('email', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group{{ $errors->first('email_confirm', ' has-error') }}">
		<label class="col-sm-3 control-label" for="email_confirm">Confirm New Email</label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="email_confirm" id="email_confirm" value="" />
			{{ $errors->first('email_confirm', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<div class="form-group{{ $errors->first('current_password', ' has-error') }}">
		<label class="col-sm-3 control-label" for="current_password">Current Password</label>
		<div class="col-sm-4">
			<input class="form-control" type="password" name="current_password" id="current_password" value="" />
			{{ $errors->first('current_password', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<hr>

	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-6">
			<button type="submit" class="btn btn-primary">Update Email</button>

			<a href="{{ route('forgot-password') }}" class="btn btn-link">I forgot my password</a>
		</div>
	</div>
</form>
@stop
