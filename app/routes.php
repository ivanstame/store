<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Register all the admin routes.
|
*/

Route::group(array('prefix' => 'admin'), function()
{

	# Blog Management
	Route::group(array('prefix' => 'blogs'), function()
	{
		Route::get('/', array('as' => 'blogs', 'uses' => 'Controllers\Admin\BlogsController@getIndex'));
		Route::get('create', array('as' => 'create/blog', 'uses' => 'Controllers\Admin\BlogsController@getCreate'));
		Route::post('create', 'Controllers\Admin\BlogsController@postCreate');
		Route::get('{blogId}/edit', array('as' => 'update/blog', 'uses' => 'Controllers\Admin\BlogsController@getEdit'));
		Route::post('{blogId}/edit', 'Controllers\Admin\BlogsController@postEdit');
		Route::get('{blogId}/delete', array('as' => 'delete/blog', 'uses' => 'Controllers\Admin\BlogsController@getDelete'));
		Route::get('{blogId}/restore', array('as' => 'restore/blog', 'uses' => 'Controllers\Admin\BlogsController@getRestore'));

		Route::get('categories', array('as' => 'blogs-categories', 'uses' => 'Controllers\Admin\BlogsController@getCategories'));
		Route::get('categories/create', array('as' => 'create/blogs-categories', 'uses' => 'Controllers\Admin\BlogsController@getCategoryCreate'));
		Route::post('categories/create', 'Controllers\Admin\BlogsController@postCategoryCreate');
		Route::get('categories/{id}/edit', array('as' => 'update/blog/category', 'uses' => 'Controllers\Admin\BlogsController@getCategoryEdit'));
		Route::post('categories/{id}/edit', 'Controllers\Admin\BlogsController@postCategoryEdit');
		Route::get('categories/{id}/delete', array('as' => 'delete/blog/category', 'uses' => 'Controllers\Admin\BlogsController@getCategoryDelete'));
		Route::get('categories/{id}/restore', array('as' => 'restore/blog/category', 'uses' => 'Controllers\Admin\BlogsController@getCategoryRestore'));
	});

	# User Management
	Route::group(array('prefix' => 'users'), function()
	{
		Route::get('/', array('as' => 'users', 'uses' => 'Controllers\Admin\UsersController@getIndex'));
		Route::get('create', array('as' => 'create/user', 'uses' => 'Controllers\Admin\UsersController@getCreate'));
		Route::post('create', 'Controllers\Admin\UsersController@postCreate');
		Route::get('{userId}/edit', array('as' => 'update/user', 'uses' => 'Controllers\Admin\UsersController@getEdit'));
		Route::post('{userId}/edit', 'Controllers\Admin\UsersController@postEdit');
		Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'Controllers\Admin\UsersController@getDelete'));
		Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'Controllers\Admin\UsersController@getRestore'));
	});

	# User Management
	Route::group(array('prefix' => 'questions'), function()
	{
		Route::get('/', array('as' => 'admin/questions', 'uses' => 'Controllers\Admin\QuestionsController@getIndex'));
		Route::get('create', array('as' => 'create/question', 'uses' => 'Controllers\Admin\QuestionsController@getCreate'));
		Route::post('create', 'Controllers\Admin\QuestionsController@postCreate');
		Route::get('{questionId}/edit', array('as' => 'update/question', 'uses' => 'Controllers\Admin\QuestionsController@getEdit'));
		Route::post('{questionId}/edit', 'Controllers\Admin\QuestionsController@postEdit');
		Route::get('{questionId}/delete', array('as' => 'delete/question', 'uses' => 'Controllers\Admin\QuestionsController@getDelete'));
		Route::get('{questionId}/restore', array('as' => 'restore/question', 'uses' => 'Controllers\Admin\QuestionsController@getRestore'));
	});

	# Group Management
	Route::group(array('prefix' => 'groups'), function()
	{
		Route::get('/', array('as' => 'groups', 'uses' => 'Controllers\Admin\GroupsController@getIndex'));
		Route::get('create', array('as' => 'create/group', 'uses' => 'Controllers\Admin\GroupsController@getCreate'));
		Route::post('create', 'Controllers\Admin\GroupsController@postCreate');
		Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'Controllers\Admin\GroupsController@getEdit'));
		Route::post('{groupId}/edit', 'Controllers\Admin\GroupsController@postEdit');
		Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'Controllers\Admin\GroupsController@getDelete'));
		Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'Controllers\Admin\GroupsController@getRestore'));
	});

	# Category Management
	Route::group(array('prefix' => 'categories'), function()
	{
		Route::get('/', array('as' => 'categories', 'uses' => 'Controllers\Admin\CategoriesController@getIndex'));
		Route::get('{catId}/edit', array('as' => 'update/category', 'uses' => 'Controllers\Admin\CategoriesController@getEdit'));
		Route::post('{catId}/edit', 'Controllers\Admin\CategoriesController@postEdit');
		Route::get('create', array('as' => 'create/category', 'uses' => 'Controllers\Admin\CategoriesController@createCategory'));
		Route::post('create', 'Controllers\Admin\CategoriesController@storeCategory');
	});

	# Category Management
	Route::group(array('prefix' => 'app'), function()
	{
		Route::get('requests', array('as' => 'app_requests', 'uses' => 'Controllers\Admin\AppController@getRequests'));
		Route::post('requests', array('uses' => 'Controllers\Admin\AppController@postRequest'));
	});

	Route::get('logins', array('as' => 'logins', 'uses' => 'Controllers\Admin\DashboardController@getLogins'));

	# Dashboard
	Route::get('/', array('as' => 'admin', 'uses' => 'Controllers\Admin\DashboardController@getIndex'));

});

/*
|--------------------------------------------------------------------------
| Authentication and Authorization Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'auth'), function()
{

	# Login
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');

	# Register
	Route::get('signup', array('as' => 'signup', 'uses' => 'AuthController@getSignup'));
	Route::post('signup', 'AuthController@postSignup');

	# Account Activation
	Route::get('activate/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));

	# Forgot Password
	Route::get('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@getForgotPassword'));
	Route::post('forgot-password', 'AuthController@postForgotPassword');

	# Forgot Password Confirmation
	Route::get('forgot-password/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

});

/*
|--------------------------------------------------------------------------
| Account Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'account'), function()
{

	# Account Dashboard
	Route::get('/', array('as' => 'account', 'uses' => 'Controllers\Account\DashboardController@getIndex'));

	# Profile
	Route::get('profile', array('as' => 'profile', 'uses' => 'Controllers\Account\ProfileController@getIndex'));
	Route::post('profile', 'Controllers\Account\ProfileController@postIndex');

	Route::get('profile/{userId}', array('as' => 'user-page', 'uses' => 'Controllers\Account\ProfileController@getProfile'));

	# Change Password
	Route::get('change-password', array('as' => 'change-password', 'uses' => 'Controllers\Account\ChangePasswordController@getIndex'));
	Route::post('change-password', 'Controllers\Account\ChangePasswordController@postIndex');

	# Change Email
	Route::get('change-email', array('as' => 'change-email', 'uses' => 'Controllers\Account\ChangeEmailController@getIndex'));
	Route::post('change-email', 'Controllers\Account\ChangeEmailController@postIndex');

	# Apply to be a developer
	Route::get('become-developer', array('as' => 'apply-dev', 'uses' => 'Controllers\Account\DashboardController@getBecomeDev'));
	Route::post('become-developer', 'Controllers\Account\DashboardController@postBecomeDev');

	Route::get('add-credit', array('as' => 'add-credit', 'uses' => 'Controllers\Account\ProfileController@getAddCredit'));

});

/*
|--------------------------------------------------------------------------
| Static Page Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::get('contact-us', array('as' => 'contact-us', 'uses' => 'ContactUsController@getIndex'));
Route::post('contact-us', 'ContactUsController@postIndex');

Route::get('about-us', array('as' => 'about-us', 'uses' => 'HomeController@getAbouUs'));

/*
|--------------------------------------------------------------------------
| Blog Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'blog'), function()
{

	Route::get('{postSlug}', array('as' => 'view-post', 'uses' => 'BlogController@getView'));
	Route::post('{postSlug}', 'BlogController@postView');

	Route::get('/', array('as' => 'blog', 'uses' => 'BlogController@getIndex'));

});

/*
|--------------------------------------------------------------------------
| Applications Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'app'), function()
{
	Route::get('submit', array('before' => array('dev-auth','project.limit'), 'as' => 'apply-app', 'uses' => 'ApplicationController@getApplyApp'));
	Route::post('submit', array('before' => array('dev-auth','project.limit'), 'uses' => 'ApplicationController@postApplyApp'));

	Route::get('{appSlug}/download', array('as' => 'download-app-blank', 'uses' => 'ApplicationController@getDownloadAppBlank'));
	Route::get('{appSlug}/buy', array('as' => 'buy-app', 'before' => 'auth', 'uses' => 'ApplicationController@getBuyApp'));
	Route::post('{appSlug}/comment', array('as' => 'comment-app', 'before' => 'auth', 'uses' => 'ApplicationController@postComment'));

	Route::get('{appSlug}/edit', array('as' => 'edit-app', 'before' => 'dev-auth', 'uses' => 'ApplicationController@getEditApp'));
	Route::post('{appSlug}/edit', array('before' => 'dev-auth', 'uses' => 'ApplicationController@postEditApp'));

	Route::get('{appSlug}/{versionId?}', array('as' => 'view-app', 'uses' => 'ApplicationController@getApp'));

	Route::get('{appSlug}/{versionId}/edit', array('as' => 'edit-version', 'before' => 'dev-auth',  'uses' => 'ApplicationController@getEditVersion'));
	Route::post('{appSlug}/{versionId}/edit', array('before' => 'dev-auth', 'uses' => 'ApplicationController@postEditVersion'));
	Route::get('{appSlug}/{versionId}/delete', array('as' => 'delete-version', 'before' => 'dev-auth',  'uses' => 'ApplicationController@getDeleteVersion'));

	Route::get('{appSlug}/{versionId}/download', array('as' => 'download-app', 'uses' => 'ApplicationController@getDownloadApp'));

	Route::get('{appSlug}/add/version', array('as' => 'add-version', 'before' => 'dev-auth', 'uses' => 'ApplicationController@getAddVerison'));
	Route::post('{appSlug}/add/version', array('before' => 'dev-auth', 'uses' => 'ApplicationController@postAddVerison'));

	Route::get('{appSlug}/add/screenshot', array('as' => 'add-screenshot', 'before' => 'dev-auth', 'uses' => 'ApplicationController@getAddScreenshot'));
	Route::post('{appSlug}/add/screenshot', array('before' => 'dev-auth', 'uses' => 'ApplicationController@postAddScreenshot'));
});

Route::group(array('prefix' => 'developer', 'before' => 'dev-auth'), function()
{
	Route::get('apps', array('as' => 'dev-apps', 'uses' => 'AppDashboardController@getApps'));
});

Validator::extend('question', function($attribute, $value, $parameters)
{
    $question = Question::where('active', '=', true)->where('id', '=', $parameters[0])->first();

    if(Str::lower($value) == $question->answer)
    {
    	$question->answered = $question->answered + 1;
    	$question->save();

    	return true;
    }
    else
    {
    	$question->failed = $question->failed + 1;
    	$question->save();

    	return false;
    }
});



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'api'), function()
{
	Route::group(array('prefix' => 'v1'), function()
	{

		Route::get('slug/{string}', array('as' => 'slugg', 'uses' => 'ApiController@getSlugged'));

	});
});

/*
|--------------------------------------------------------------------------
| Master categories
|--------------------------------------------------------------------------
*/

Route::get('windows', array('uses' => 'HomeController@getWindows'));
Route::get('linux', array('uses' => 'HomeController@getLinux'));
Route::get('mac', array('uses' => 'HomeController@getMac'));



/*
|--------------------------------------------------------------------------
| Search
|--------------------------------------------------------------------------
*/

Route::get('search/{query}', array('as' => 'search_do', 'uses' => 'HomeController@getSearch'));
Route::post('search', array('as' => 'search_pre', 'uses' => 'HomeController@postSearch'));

Route::get('/download/{downloadString}/{from?}', array('as' => 'download', 'uses' => 'ApplicationController@getDownload'));
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@getIndex'));
Route::get('{parent}/{child}', array('uses' => 'HomeController@getCategory'));

Route::get('testing', function()
{

});
