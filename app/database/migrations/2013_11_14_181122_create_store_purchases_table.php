<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStorePurchasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_purchases', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('app_id');
			$table->integer('discount_id');
			$table->integer('coupon_id');
			$table->integer('serial_id');
			$table->integer('version_id');
			$table->float('price');
			$table->string('pay_from_email', 255);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_purchases');
	}

}
