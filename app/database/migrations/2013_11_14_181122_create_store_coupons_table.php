<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreCouponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_coupons', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('app_id');
			$table->string('type', 8);
			$table->string('code', 8);
			$table->integer('use_limits');
			$table->datetime('dt_end');
			$table->integer('used');
			$table->float('how_much');
			$table->integer('percentage');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_coupons');
	}

}
