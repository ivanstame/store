<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreDownloadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_downloads', function(Blueprint $table) {
			$table->increments('id');
			$table->string('download_string', 255);
			$table->integer('version_id');
			$table->integer('user_id');
			$table->integer('downloads');
			$table->integer('downloads_client');
			$table->string('ip', 15);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_downloads');
	}

}
