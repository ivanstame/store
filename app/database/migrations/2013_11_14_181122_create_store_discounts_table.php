<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreDiscountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_discounts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('app_id');
			$table->integer('percentage');
			$table->datetime('dt_start');
			$table->datetime('dt_end');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_discounts');
	}

}
