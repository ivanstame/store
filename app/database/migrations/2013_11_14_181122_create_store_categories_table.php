<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 32);
			$table->string('slug', 64);
			$table->integer('parent_id');
			$table->string('description', 255);
			$table->text('keywords');
			$table->softDeletes();
			$table->boolean('top_level');
			$table->boolean('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_categories');
	}

}
