<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumForumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_forums', function(Blueprint $table) {
			$table->increments('id');
			$table->string('forum_name', 80);
			$table->text('forum_desc')->nullable();
			$table->string('redirect_url', 100)->nullable();
			$table->integer('last_post_id')->nullable();
			$table->integer('last_poster_id')->nullable();
			$table->integer('disp_position');
			$table->integer('category_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_forums');
	}

}
