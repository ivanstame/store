<?php

use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Update the users table
		Schema::table('users', function($table)
		{
			$table->softDeletes();
			$table->string('country')->nullable();
			$table->string('gravatar')->nullable();
			$table->string('username', 16)->unique();

			$table->float('balance');
            $table->integer('project_limits');
            $table->integer('last_authentication_id');
            $table->string('short_info');
            $table->text('website');
            $table->string('company', 32);
            $table->text('f_key');
            $table->string('forum_title', 50)->nullable()->default('Member');
		});

		Schema::create('store_bans', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('ip', 15);
			$table->string('email', 250);
			$table->string('message', 255);
			$table->integer('expire')->unsigned();
			$table->integer('ban_creator_id')->unsigned();
			$table->timestamps();
		});

        Schema::create('store_authentications', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('login_ip', 15);
			$table->timestamps();
		});

        Schema::create('store_questions', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('question');
			$table->string('answer');
			$table->boolean('active')->default(true);
			$table->integer('answered');
			$table->integer('failed');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Update the users table
		Schema::table('users', function($table)
		{
			$table->dropColumn('deleted_at', 'website', 'country', 'gravatar');
		});
		Schema::drop('store_questions');
		Schema::drop('store_authentications');
		Schema::drop('store_bans');
	}

}
