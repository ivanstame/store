<?php

use Illuminate\Database\Migrations\Migration;

class CreateForumTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_categories', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('name', 32);
            $table->string('slug', 64);
            $table->string('description');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('forum_permissions', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('forum_id');
			$table->boolean('read_forum');
			$table->boolean('post_replies');
            $table->boolean('post_topics');
		    $table->timestamps();
		});

		Schema::create('forum_subscriptions', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('app_id')->unsigned();
			$table->integer('forum_id')->unsigned();
		    $table->timestamps();
		});

		Schema::create('topic_subscriptions', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('app_id')->unsigned();
			$table->integer('topic_id')->unsigned();
		    $table->timestamps();
		});

		Schema::create('forum_forums', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('forum_name', 80);
			$table->text('forum_desc')->nullable();
			$table->string('redirect_url', 100)->nullable();
			$table->integer('last_post_id')->unsigned()->nullable();
			$table->integer('last_poster_id')->unsigned()->nullable();
			$table->integer('disp_position')->default(0);
			$table->integer('category_id')->unsigned();
		    $table->timestamps();
		});

		Schema::create('forum_topics', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('first_post_id')->unsigned()->default(0);
			$table->integer('last_post_id')->unsigned()->default(0);
			$table->integer('last_poster_id')->unsigned()->default(0);
			$table->integer('num_views')->unsigned()->default(0);
			$table->boolean('closed')->default(false);
			$table->boolean('sticky')->default(false);
			$table->integer('moved_to')->unsigned()->nullable();
			$table->integer('forum_id')->unsigned()->default(0);
		    $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('forum_reports', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('post_id')->unsigned()->default(0);
			$table->integer('topic_id')->unsigned()->default(0);
			$table->integer('forum_id')->unsigned()->default(0);
			$table->integer('reported_by')->unsigned()->default(0);
			$table->text('message')->nullable();
		    $table->timestamps();
		});

		Schema::create('forum_posts', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('poster_id')->unsigned()->default(1);
			$table->string('poster_ip', 15)->nullable();
			$table->string('poster_email', 250)->nullable();
			$table->text('message')->nullable();
			$table->boolean('hide_smilies')->default(false);
			$table->integer('edited_by')->nullable();
			$table->integer('topic_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_posts');
		Schema::drop('forum_reports');
		Schema::drop('forum_topics');
		Schema::drop('forum_forums');
		Schema::drop('topic_subscriptions');
		Schema::drop('forum_subscriptions');
		Schema::drop('forum_permissions');
		Schema::drop('forum_categories');
	}

}
