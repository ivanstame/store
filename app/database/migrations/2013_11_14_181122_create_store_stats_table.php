<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_stats', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('app_id');
			$table->integer('version_id');
			$table->string('type', 16);
			$table->integer('number_of_screens');
			$table->integer('screen1_width');
			$table->integer('screen1_height');
			$table->integer('screen2_width');
			$table->integer('screen2_height');
			$table->integer('physical_memory');
			$table->integer('physical_memory_speed');
			$table->string('memory_type', 8);
			$table->string('cpu_name', 64);
			$table->float('cpu_speed');
			$table->integer('cpu_cores');
			$table->integer('cpu_threads');
			$table->integer('cpu_l1');
			$table->integer('cpu_l2');
			$table->integer('cpu_l3');
			$table->string('system_name', 32);
			$table->string('system_version', 16);
			$table->date('system_install_date');
			$table->string('gpu_name', 64);
			$table->float('gpu_speed');
			$table->integer('gpu_memory');
			$table->string('gpu_memory_type', 8);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_stats');
	}

}
