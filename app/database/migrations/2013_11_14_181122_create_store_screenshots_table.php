<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreScreenshotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_screenshots', function(Blueprint $table) {
			$table->increments('id');
			$table->string('caption', 32);
			$table->integer('app_id');
			$table->string('filename', 255);
			$table->integer('order');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_screenshots');
	}

}
