<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_questions', function(Blueprint $table) {
			$table->increments('id');
			$table->string('question', 255);
			$table->string('answer', 255);
			$table->boolean('active');
			$table->integer('answered');
			$table->integer('failed');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_questions');
	}

}
