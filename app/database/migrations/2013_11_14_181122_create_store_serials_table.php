<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreSerialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_serials', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('app_id');
			$table->integer('user_id');
			$table->integer('batch_id');
			$table->boolean('used');
			$table->text('key');
			$table->string('import_method', 20);
			$table->boolean('multiple');
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::create('store_serial_batches', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('serial_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_serials');
	}

}
