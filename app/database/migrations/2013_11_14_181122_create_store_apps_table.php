<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreAppsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_apps', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id');
			$table->integer('user_id');
			$table->integer('topic_id');
			$table->string('name', 64);
			$table->string('short_info', 255);
			$table->text('info');
			$table->boolean('need_key');
			$table->boolean('featured');
			$table->integer('version_id');
			$table->string('developer', 32);
			$table->text('icon');
			$table->float('price');
			$table->text('website');
			$table->string('dev_email', 255);
			$table->boolean('approved');
			$table->integer('downloads');
			$table->integer('views');
			$table->string('slug', 128);
			$table->text('tags');
			$table->datetime('updated');
			$table->boolean('is_in_client');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_apps');
	}

}
