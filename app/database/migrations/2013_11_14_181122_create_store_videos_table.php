<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_videos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('caption', 32);
			$table->string('description', 255);
			$table->integer('app_id');
			$table->string('url', 255);
			$table->integer('order');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_videos');
	}

}
