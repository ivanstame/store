<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_comments', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('app_id');
			$table->text('content');
			$table->integer('parent_id');
			$table->softDeletes();
			$table->string('ip', 15);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_comments');
	}

}
