<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStorePaypalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_paypal', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->float('amount');
			$table->string('trans_id', 255);
			$table->integer('customer_id');
			$table->string('pay_from_email', 255);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_paypal');
	}

}
