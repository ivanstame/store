<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_reports', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('app_id');
			$table->string('caption', 32);
			$table->string('body', 255);
			$table->string('category', 16);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_reports');
	}

}
