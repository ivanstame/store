<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_versions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('app_id');
			$table->string('version', 32);
			$table->boolean('architecture');
			$table->text('changelog');
			$table->string('licence', 16);
			$table->string('silent_arg', 16);
			$table->string('original_filename', 255);
			$table->string('filename', 255);
			$table->integer('size');
			$table->string('md5', 32);
			$table->integer('order');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_versions');
	}

}
