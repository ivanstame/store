<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_posts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('series_id');
			$table->integer('series_order');
			$table->string('title', 250);
			$table->string('slug', 250);
			$table->string('meta_title', 100);
			$table->string('meta_description', 250);
			$table->string('meta_keywords', 250);
			$table->text('content');
			$table->boolean('published');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_posts');
	}

}
