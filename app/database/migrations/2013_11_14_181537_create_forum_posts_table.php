<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_posts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('poster_id');
			$table->string('poster_ip', 15)->nullable();
			$table->string('poster_email', 250)->nullable();
			$table->text('message')->nullable();
			$table->boolean('hide_smilies');
			$table->integer('edited_by')->nullable();
			$table->integer('topic_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_posts');
	}

}
