<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_reports', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('post_id');
			$table->integer('topic_id');
			$table->integer('forum_id');
			$table->integer('reported_by');
			$table->text('message')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_reports');
	}

}
