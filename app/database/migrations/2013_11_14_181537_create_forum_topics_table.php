<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_topics', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('first_post_id');
			$table->integer('last_post_id');
			$table->integer('last_poster_id');
			$table->integer('num_views');
			$table->boolean('closed');
			$table->boolean('sticky');
			$table->integer('moved_to')->nullable();
			$table->integer('forum_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_topics');
	}

}
