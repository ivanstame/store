<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreBugsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_bugs', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('app_id');
			$table->integer('version_id');
			$table->string('caption', 32);
			$table->text('body');
			$table->integer('priority');
			$table->boolean('fixed');
			$table->string('ip', 15);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_bugs');
	}

}
