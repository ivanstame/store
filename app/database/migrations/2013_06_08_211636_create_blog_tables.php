<?php

use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_series', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->string('title')->length(100);
			$table->timestamps();
		});

		Schema::create('blog_posts', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('series_id')->unsigned();
			$table->smallInteger('series_order')->unsigned()->default(0);
			$table->string('title')->length(250);
			$table->string('slug')->length(250);
			$table->string('meta_title')->length(100);
			$table->string('meta_description')->length(250);
			$table->string('meta_keywords')->length(250);
			$table->text('content');
			$table->boolean('published')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::create('blog_tags', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->string('name')->length(50);
			$table->string('slug')->length(50);
			$table->timestamps();
		});

		Schema::create('blog_categories', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->string('name')->length(50);
			$table->string('slug')->length(50);
			$table->timestamps();
		});

		Schema::create('blog_post_tag', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('post_id')->unsigned();
			$table->integer('tag_id')->unsigned();
		});

		Schema::create('blog_category_post', function ($table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned();
			$table->integer('post_id')->unsigned();
		});

		Schema::create('blog_comments', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('post_id')->unsigned();
			$table->text('content');
			$table->integer('parent_id')->unsigned();
            $table->softDeletes();
            $table->string('ip', 15);
			$table->timestamps();
		});

		Schema::create('blog_views', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('user_ip', 15)->default("0.0.0.0");
		    $table->integer('user_id')->default(-1);
		    $table->integer('post_id')->unsigned();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_series');
		Schema::drop('blog_posts');
		Schema::drop('blog_tags');
		Schema::drop('blog_categories');
		Schema::drop('blog_post_tag');
		Schema::drop('blog_category_post');
		Schema::drop('blog_comments');
		Schema::drop('blog_views');
	}

}
