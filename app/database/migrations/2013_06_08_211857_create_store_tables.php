<?php

use Illuminate\Database\Migrations\Migration;

class CreateStoreTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_apps', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('topic_id')->unsigned();
            $table->string('name', 64)->unique();
            $table->string('short_info');
            $table->text('info');
            $table->boolean('free');
            $table->boolean('need_key');
            $table->boolean('featured');
            $table->integer('version_id');
            $table->string('developer', 32);
            $table->text('icon');
            $table->float('price');
            $table->text('website');
            $table->string('dev_email');
            $table->boolean('approved');
            $table->string('slug', 128);
            $table->text('tags');
            $table->timestamp('updated');
            $table->boolean('is_in_client');
            $table->integer('downloads');
            $table->integer('views');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_bugs', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->integer('version_id')->unsigned();
            $table->string('caption', 32);
            $table->text('body');
            $table->integer('priority');
            $table->boolean('fixed');
            $table->string('ip', 15);
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_coupons', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('app_id')->unsigned();
            $table->string('type', 8);
            $table->string('code', 8)->unique();
            $table->integer('use_limits');
            $table->timestamp('dt_end');
            $table->integer('used');
            $table->float('how_much');
            $table->integer('percentage');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_discounts', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('app_id')->unsigned();
            $table->integer('percentage');
            $table->timestamp('dt_start');
            $table->timestamp('dt_end');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_downloads', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('download_string');
            $table->integer('version_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('downloads');
            $table->integer('downloads_client');
            $table->string('ip', 15);
		    $table->timestamps();
		});

		Schema::create('store_paypal', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
            $table->float('amount');
            $table->string('trans_id');
            $table->integer('customer_id');
            $table->string('pay_from_email');
		    $table->timestamps();
		});

		Schema::create('store_purchases', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->integer('discount_id')->unsigned();
            $table->integer('coupon_id')->unsigned();
            $table->integer('serial_id')->unsigned();
            $table->integer('version_id')->unsigned();
            $table->float('price');
            $table->string('pay_from_email');
		    $table->timestamps();
		});

		Schema::create('store_reports', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->string('caption', 32);
            $table->string('body');
            $table->string('category', 16);
		    $table->timestamps();
		});

		Schema::create('store_rewards', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('app_id')->unsigned();
            $table->string('type', 16);
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_screenshots', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('caption', 32);
            $table->string('description');
            $table->integer('app_id');
            $table->string('filename');
            $table->integer('order');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_searches', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->default(0)->unsigned();
            $table->string('ip', 15);
            $table->string('query');
		    $table->timestamps();
		});

		Schema::create('store_serials', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('app_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('used');
            $table->text('key');
            $table->string('import_method', 20);
            $table->boolean('multiple');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_versions', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('app_id')->unsigned();
            $table->string('version', 32);
            $table->text('changelog');
            $table->string('licence', 16);
            $table->string('silent_arg', 16);
            $table->string('original_filename');
            $table->string('filename');
            $table->integer('size');
            $table->string('md5', 32);
            $table->integer('order');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_categories', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('name', 32);
            $table->string('slug', 64);
            $table->integer('parent_id')->default(0)->unsigned();
            $table->string('description');
            $table->softDeletes();
            $table->boolean('top_level');
		    $table->timestamps();
		});

		Schema::create('store_videos', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('caption', 32);
            $table->string('description');
            $table->integer('app_id')->unsigned();
            $table->string('url');
            $table->integer('order');
            $table->softDeletes();
		    $table->timestamps();
		});

		Schema::create('store_stats', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->default(0)->unsigned();
            $table->integer('app_id')->default(0)->unsigned();
            $table->integer('version_id')->default(-1);
            $table->string('type', 16);
            $table->integer('number_of_screens');
            $table->integer('screen1_width');
            $table->integer('screen1_height');
            $table->integer('screen2_width');
            $table->integer('screen2_height');
            $table->integer('physical_memory');
            $table->integer('physical_memory_speed');
            $table->string('memory_type', 8);
            $table->string('cpu_name', 64);
            $table->float('cpu_speed');
            $table->integer('cpu_cores');
            $table->integer('cpu_threads');
            $table->integer('cpu_l1');
            $table->integer('cpu_l2');
            $table->integer('cpu_l3');
            $table->string('system_name', 32);
            $table->string('system_version', 16);
            $table->date('system_install_date');
            $table->string('gpu_name', 64);
            $table->float('gpu_speed');
            $table->integer('gpu_memory');
            $table->string('gpu_memory_type', 8);
		    $table->timestamps();
		});

		Schema::create('store_views', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('user_ip', 15)->default("0.0.0.0");
		    $table->integer('user_id')->default(0)->unsigned();
		    $table->integer('app_id')->unsigned();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_apps');
		Schema::drop('store_bugs');
		Schema::drop('store_coupons');
		Schema::drop('store_discounts');
		Schema::drop('store_downloads');
		Schema::drop('store_paypal');
		Schema::drop('store_purchases');
		Schema::drop('store_reports');
		Schema::drop('store_rewards');
		Schema::drop('store_screenshots');
		Schema::drop('store_searches');
		Schema::drop('store_serials');
		Schema::drop('store_versions');
		Schema::drop('store_categories');
		Schema::drop('store_videos');
		Schema::drop('store_stats');
		Schema::drop('store_views');
	}

}
