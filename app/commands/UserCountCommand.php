<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UserCountCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'user:count';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get the user statistics';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$users = Sentry::getUserProvider()->createModel();
		$this->comment('=====================================');
		$this->comment('There are ' . $users->withTrashed()->count() . ' users.');
		$this->comment('-------------------------------------');
		$this->comment('There are ' . $users->onlyTrashed()->count() . ' trashed users.');
		$this->comment('=====================================');
	}
}