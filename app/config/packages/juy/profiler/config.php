<?php

return array(

	// Set this option to false if you are already including jquery in your application to avoid conflicts.
    'jquery' => false,

	// Set to true to enable profiling, false to disable
    'profiler' => false
);
