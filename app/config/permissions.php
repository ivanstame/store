<?php

return array(

	'Global' => array(
		array(
			'permission' => 'superuser',
			'label'      => 'Super User',
		),
	),

	'Admin' => array(
		array(
			'permission' => 'admin',
			'label'      => 'Admin Rights',
		),
	),

	'Owner' => array(
		array(
			'permission' => 'owner',
			'label'      => 'Owner Rights',
		),
	),

	'Developer' => array(
		array(
			'permission' => 'developer',
			'label'      => 'Developer Rights',
		),
	),

	'Member' => array(
		array(
			'permission' => 'member',
			'label'      => 'Member Rights',
		),
	),

);
