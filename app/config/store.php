<?php

return array(

	'name' => 'Zogitech',

	'title' => 'Zogitech',

	'keywords' => 'Zogitech',

	'description' => 'Zogitech',

	'slogan' => 'We take care of our own!',


    'modules' => array(
        'register'          => true,
        'login'             => true,
        'app_submission'    => true,
        'downloads'         => true
    )
);
