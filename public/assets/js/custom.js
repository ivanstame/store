var hash = window.location.hash,
hashPart=hash.split('!')[1],
activeTab=$('ul.nav a[href="#' + hashPart + '"]');
activeTab && activeTab.tab('show');

$('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    window.location.hash = '#!'+$(this).attr('href').split('#')[1];
});
